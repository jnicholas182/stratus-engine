#ifndef BUFFERFACTORY_H
#define BUFFERFACTORY_H

#include <vector>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <Utils\DataStructures.h>

class D3D11Graphics;

class BufferFactory
{
public:
	//--------------------------------------------------------------------------------------
	// Create a vertex buffer based on the vector of vertices supplied
	//--------------------------------------------------------------------------------------
	template<typename T> static HRESULT CreateVertexBuffer(std::vector<T> vertices, ID3D11Buffer** vertexBuffer)
	{
		// Fill a buffer description
		D3D11_BUFFER_DESC bufferDesc;
		ZeroMemory(&bufferDesc, sizeof(bufferDesc));
		bufferDesc.ByteWidth = sizeof(T) * vertices.size();
		bufferDesc.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bufferDesc.CPUAccessFlags = 0;

		// Define the resource data
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = vertices.data();

		// Create the buffer
		HRESULT hr = D3D11Graphics::getInstance().getDevice()->CreateBuffer(&bufferDesc, &InitData, vertexBuffer);
		if (FAILED(hr))
			return hr;
		return S_OK;
	}

	static HRESULT CreateIndexBuffer(std::vector<unsigned int> indices, ID3D11Buffer** indexBuffer);

	//--------------------------------------------------------------------------------------
	// Create a constant buffer of a templated type
	//--------------------------------------------------------------------------------------
	template<typename T> static HRESULT CreateConstantBuffer(ID3D11Buffer** constantBuffer)
	{
		// Fill in a buffer description.
		D3D11_BUFFER_DESC bufferDesc;
		ZeroMemory(&bufferDesc, sizeof(bufferDesc));
		bufferDesc.ByteWidth = sizeof(T);
		bufferDesc.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bufferDesc.CPUAccessFlags = 0;

		// Create the buffer
		HRESULT hr = D3D11Graphics::getInstance().getDevice()->CreateBuffer(&bufferDesc, NULL, constantBuffer);
		if (FAILED(hr))
			return hr;

		return S_OK;
	}
};

#endif