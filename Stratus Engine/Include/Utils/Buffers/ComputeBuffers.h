#ifndef COMPUTEBUFFERS_H
#define COMPUTEBUFFERS_H

#include <vector>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <Utils\DataStructures.h>

class D3D11Graphics;

class ComputeBuffers
{
public:
	static void ClearBuffers();
};

class ComputeInputBuffer
{
public:
	ComputeInputBuffer();
	template<class T>HRESULT Initialise(UINT elementCount, UINT stride, T* data)
	{
		if (m_srcDataGPUBuffer)
			m_srcDataGPUBuffer->Release();
		m_srcDataGPUBuffer = NULL;

		// First we create a buffer in GPU memory
		D3D11_BUFFER_DESC descGPUBuffer;
		ZeroMemory(&descGPUBuffer, sizeof(descGPUBuffer));
		descGPUBuffer.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
		descGPUBuffer.ByteWidth = elementCount * stride;
		descGPUBuffer.Usage = D3D11_USAGE_DEFAULT;
		descGPUBuffer.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
		descGPUBuffer.StructureByteStride = stride;

		D3D11_SUBRESOURCE_DATA bufferInitData; 
		ZeroMemory(&bufferInitData, sizeof(bufferInitData));
		bufferInitData.SysMemPitch = stride;
		bufferInitData.pSysMem = data ? data : nullptr;
		bufferInitData.SysMemSlicePitch = elementCount*stride;
		if (FAILED(D3D11Graphics::getInstance().getDevice()->CreateBuffer(&descGPUBuffer, data ? &bufferInitData : nullptr, &m_srcDataGPUBuffer)))
			return E_FAIL;

		// Now we create a view on the resource. DX11 requires you to send the data to shaders using a "shader view"
		D3D11_BUFFER_DESC descBuf;
		ZeroMemory(&descBuf, sizeof(descBuf));
		m_srcDataGPUBuffer->GetDesc(&descBuf);

		D3D11_SHADER_RESOURCE_VIEW_DESC descView;
		ZeroMemory(&descView, sizeof(descView));
		descView.Format = DXGI_FORMAT_UNKNOWN;
		descView.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
		descView.BufferEx.FirstElement = 0;
		descView.BufferEx.NumElements = elementCount;
		descView.Buffer.ElementWidth = stride;
		descView.Buffer.NumElements = elementCount;

		if (FAILED(D3D11Graphics::getInstance().getDevice()->CreateShaderResourceView(m_srcDataGPUBuffer, &descView, &m_srcDataGPUBufferView)))
			return E_FAIL;

		return S_OK;
	}

	void SetActive(int bufferRegistration);
	void CleanUp();

	ID3D11Buffer*				m_srcDataGPUBuffer;
	ID3D11ShaderResourceView*	m_srcDataGPUBufferView;
};

class ComputeOutputBuffer
{
public:
	ComputeOutputBuffer();
	HRESULT Initialise(UINT elementCount, UINT stride);
	void SetActive();
	void CleanUp();

	template<class T> HRESULT getGPUDestBufferCopy(T* outBuff)
	{
		ID3D11Buffer* debugbuf = NULL;

		D3D11_BUFFER_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		m_destDataGPUBuffer->GetDesc(&desc);

		desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
		desc.Usage = D3D11_USAGE_STAGING;
		desc.BindFlags = 0;
		desc.MiscFlags = 0;

		if (FAILED(D3D11Graphics::getInstance().getDevice()->CreateBuffer(&desc, NULL, &debugbuf)))
			return E_FAIL;

		D3D11Graphics::getInstance().getContext()->CopyResource(debugbuf, m_destDataGPUBuffer);

		D3D11_MAPPED_SUBRESOURCE mappedResource;
		if (FAILED(D3D11Graphics::getInstance().getContext()->Map(debugbuf, 0, D3D11_MAP_READ, 0, &mappedResource)))
			return E_FAIL;

		memcpy(outBuff, mappedResource.pData, byteSize);

		D3D11Graphics::getInstance().getContext()->Unmap(debugbuf, 0);

		debugbuf->Release();
		
		return S_OK;
	}

	ID3D11Buffer*				m_destDataGPUBuffer;
	ID3D11UnorderedAccessView*	m_destDataGPUBufferView;

	UINT byteSize;
};

#endif