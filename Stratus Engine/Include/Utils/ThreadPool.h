#ifndef THREADPOOL_H
#define THREADPOOL_H

#define _WIN32_WINDOWS 1
#define BOOST_ASIO_NO_WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <boost\asio\io_service.hpp>
#include <boost\thread\thread.hpp>
#include <boost\function.hpp>

class ThreadPool 
{
public:
	ThreadPool() :service(), work(service)
	{
		availableThreads = 3;
		for (std::size_t i = 0; i < availableThreads; i++)
		{
			threadGroup.create_thread(boost::bind(&boost::asio::io_service::run, &(service)));
		}
	}

	void joinAll()
	{
		threadGroup.join_all();
	}

	template<class Task> void enqueue(Task task)
	{
		boost::unique_lock<boost::mutex> lock(poolMutex);

		if (availableThreads == 0)
			return;

		--availableThreads;
		service.post(task);
		
		++availableThreads;
	}

	~ThreadPool() 
	{
		service.stop();
		threadGroup.join_all();	
	}

private:
	boost::asio::io_service service;
	boost::asio::io_service::work work;
	boost::thread_group threadGroup;
	std::size_t availableThreads;
	boost::mutex poolMutex;
};

#endif