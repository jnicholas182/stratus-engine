#ifndef DATASTRUCTURES_H
#define DATASTRUCTURES_H

#include <directxmath.h>

const float CAM_MOVE_SPEED = 3.0f;

struct float3
{
public:
	float x, y, z;
	float3() :x(0), y(0), z(0){}
	float3(float x, float y, float z)
		:
		x(x),
		y(y),
		z(z)
	{
	}

	float3 operator*(const float3& l) 
	{
		return float3(l.x*x, l.y*y, l.z*z);
	}
};

enum FallType
{
	Light,
	Heavy,
	Torrential
};

//--------------------------------------------------------------------------------------
// Basic vertex data structure
//--------------------------------------------------------------------------------------
struct Vertex
{
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT3 Normal;
	DirectX::XMFLOAT2 TexCoord;

	Vertex(DirectX::XMFLOAT3 Position, DirectX::XMFLOAT3 Normal, DirectX::XMFLOAT2 TexCoord)
	{
		this->Position = Position;
		this->Normal = Normal;
		this->TexCoord = TexCoord;
	}
};

//--------------------------------------------------------------------------------------
// Vertex structure to be used by Screen Space objects
//--------------------------------------------------------------------------------------
struct ScreenSpaceVertex
{
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT2 TexCoord;
};

//--------------------------------------------------------------------------------------
// World matrix data structure
//--------------------------------------------------------------------------------------
struct WorldMatrix
{
	DirectX::XMMATRIX World;
};

//--------------------------------------------------------------------------------------
// View matrix data structure
//--------------------------------------------------------------------------------------
struct ViewMatrix
{
	DirectX::XMMATRIX View;
};

//--------------------------------------------------------------------------------------
// Projection matrix data structure
//--------------------------------------------------------------------------------------
struct ProjectionMatrix
{
	DirectX::XMMATRIX Projection;
};

//--------------------------------------------------------------------------------------
// Camera data structure
//--------------------------------------------------------------------------------------
struct CameraData
{
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT3 Direction;
	DirectX::XMFLOAT3 Orientation;
	DirectX::XMFLOAT3 Up;
};

//--------------------------------------------------------------------------------------
// Camera position data structure to be passed to shaders
//--------------------------------------------------------------------------------------
struct CameraConstBuffer
{
	DirectX::XMFLOAT3 Position;
	float pad;
};

//--------------------------------------------------------------------------------------
// Data structure containing light data for second pass of rendering
//--------------------------------------------------------------------------------------
struct LightData
{
	DirectX::XMFLOAT4 Position[16];
	DirectX::XMFLOAT4 Direction[16];
	DirectX::XMFLOAT4 Diffuse[16];
	DirectX::XMFLOAT4 Specular[16];
	DirectX::XMINT4 type[16];
	float cone[16];
	int lightCount;
	int pad[3];
};

//--------------------------------------------------------------------------------------
// Prototyping of operators for XMFLOAT3
//--------------------------------------------------------------------------------------
DirectX::XMFLOAT3 operator*(DirectX::XMFLOAT3 l, float r);
DirectX::XMFLOAT3 operator*(float l, DirectX::XMFLOAT3 r);
DirectX::XMFLOAT3 operator/(DirectX::XMFLOAT3 l, float r);
DirectX::XMFLOAT3 operator+(DirectX::XMFLOAT3 l, DirectX::XMFLOAT3 r);
DirectX::XMFLOAT3 operator-(DirectX::XMFLOAT3 l, DirectX::XMFLOAT3 r);
void operator+=(DirectX::XMFLOAT3& l, DirectX::XMFLOAT3 r);
float lerp(float a, float b, float t);

DirectX::XMFLOAT2 operator-(DirectX::XMFLOAT2 l, DirectX::XMFLOAT2 r);
DirectX::XMFLOAT2 NormalizeXMFLOAT2(DirectX::XMFLOAT2 vector);
DirectX::XMFLOAT2 operator*(DirectX::XMFLOAT2 l, float r);
DirectX::XMFLOAT2 operator*(float l, DirectX::XMFLOAT2 r);
DirectX::XMFLOAT2 operator+(DirectX::XMFLOAT2 l, DirectX::XMFLOAT2 r);

DirectX::XMFLOAT3 NormalizeXMFLOAT3(DirectX::XMFLOAT3 vector);
float Dot(DirectX::XMFLOAT3 first, DirectX::XMFLOAT3 second);
DirectX::XMFLOAT3 Cross(DirectX::XMFLOAT3 first, DirectX::XMFLOAT3 second);
float Magnitude(DirectX::XMFLOAT3 vector);
float Magnitude(DirectX::XMFLOAT2 vector);

float randomFloat(float minBound, float maxBound);

#endif