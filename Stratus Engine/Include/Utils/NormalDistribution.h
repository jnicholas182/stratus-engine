#ifndef NORMALDISTRIBUTION_H
#define NORMALDISTRIBUTION_H

#include <random>
#include <Utils\DataStructures.h>

class NormalDistribution
{
public:
	static float GetRandomValue(float minBound, float maxBound)
	{
		std::random_device device;
		std::mt19937 generator(device());
		std::normal_distribution<> dist(0.5f, 0.2f);

		return lerp(minBound, maxBound, (float)dist(generator));
	}
};

#endif