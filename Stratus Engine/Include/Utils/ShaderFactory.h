#ifndef SHADER_H
#define SHADER_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <string>
#include <Utils\resource.h>
#include <Graphics\D3D11Graphics.h>

class ShaderFactory
{
public:
	static HRESULT CreateVertexShader(ID3D11VertexShader** shader, std::wstring shaderFile, std::string shaderName, D3D11_INPUT_ELEMENT_DESC* layout, UINT numElements, ID3D11InputLayout** vertexLayout);
	
	template<typename T> static HRESULT CreateShader(T** shader, std::wstring shaderFile, std::string shaderName, std::string shaderModel)
	{
		ID3DBlob* shaderBlob = NULL;

		// Compile shader
		if (FAILED(CompileShaderFromFile(shaderFile.c_str(), shaderName.c_str(), shaderModel.c_str(), &shaderBlob)))
		{
			MessageBox(NULL, "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK);
			return E_FAIL;
		}

		// Create shader
		HRESULT shaderCreationResult;

		if (typeid(T) == typeid(ID3D11PixelShader))
			shaderCreationResult = D3D11Graphics::getInstance().getDevice()->CreatePixelShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), NULL, (ID3D11PixelShader**)shader);
		else if (typeid(T) == typeid(ID3D11GeometryShader))
			shaderCreationResult = D3D11Graphics::getInstance().getDevice()->CreateGeometryShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), NULL, (ID3D11GeometryShader**)shader);
		else if (typeid(T) == typeid(ID3D11ComputeShader))
			shaderCreationResult = D3D11Graphics::getInstance().getDevice()->CreateComputeShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), NULL, (ID3D11ComputeShader**)shader);
		else if (typeid(T) == typeid(ID3D11DomainShader))
			shaderCreationResult = D3D11Graphics::getInstance().getDevice()->CreateDomainShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), NULL, (ID3D11DomainShader**)shader);
		else if (typeid(T) == typeid(ID3D11HullShader))
			shaderCreationResult = D3D11Graphics::getInstance().getDevice()->CreateHullShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), NULL, (ID3D11HullShader**)shader);
		else
			return E_FAIL;

		// Check for failed shader creation
		if (FAILED(shaderCreationResult))
		{
			shaderBlob->Release();
			return E_FAIL;
		}
		return S_OK;
	}
private:
	static HRESULT CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);
};

#endif