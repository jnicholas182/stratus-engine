#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <chrono>
#include <ctime>
#include <string>

using namespace std::chrono;

class Stopwatch
{
public:
	Stopwatch();
	void Start();
	void Stop();
	float getAverage();
	int getIterations();
private:
	time_point<system_clock> start, end;
	int iterations;
	float timeTotal;
};

#endif