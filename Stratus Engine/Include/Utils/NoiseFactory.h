#ifndef PERLINFACTORY_H
#define PERLINFACTORY_H

#include <vector>
#include <Utils\DataStructures.h>

class SimplexNoise
{
public:
	std::vector<int> permutation;
	SimplexNoise(unsigned int seed, float cover, float sharpness);
	double getNoiseValue(int octaves, float persistence, float scale, double x, double y, double z);
	double raw3DNoise(double x, double y, double z);
private:
	double fade(double t);
	double lerp(double t, double a, double b);
	double grad(int hash, double x, double y, double z);
	double exponentialFilter(float perlinValue);

	float cover;
	float sharpness;
};

#endif