#ifndef GAME_H
#define GAME_H

class D3D11Graphics;
class Gamepad;
class Light;
class GameObject;
class SkyboxObject;

class Game
{
public:
	Game();
	~Game();
	HRESULT Initialise();
	MSG Run(HWND g_hWnd);
	void CleanUp();
	HRESULT SetupGameObjects();
	HRESULT SetupLights();
	void AddLight(LightData* p_this, DirectX::XMFLOAT4 Position, DirectX::XMFLOAT4 Direction, DirectX::XMFLOAT4 Colour, int type, float cone = 0.0f);

private:
	std::vector<GameObject*> gameObjects;
	SkyboxObject* skyBox;

	LightData* lights;
	ID3D11Buffer* LightDataBuffer = NULL;
	ID3D11Buffer* previousFrameDurationBuffer = NULL;

	Gamepad* gamepad;
};

#endif
