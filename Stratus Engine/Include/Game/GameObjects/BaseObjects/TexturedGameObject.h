#ifndef TEXTUREDGAMEOBJECT_H
#define TEXTUREDGAMEOBJECT_H

class GameObject;

class TexturedGameObject : public GameObject
{
public:
	TexturedGameObject();
	HRESULT Initialise(const std::string& FileName, DirectX::XMFLOAT3 translation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT3 rotation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), float scale = 1.0f);
	void SetShadersAndBuffers();
	void Render();
	void WriteDataToFile(std::string fileName);
	void CleanUp();

private:
	ID3D11ShaderResourceView* texture = NULL;
	std::vector<Vertex> vertices;
};

#endif