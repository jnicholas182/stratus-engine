#ifndef SKYBOXOBJECT_H
#define SKYBOXOBJECT_H

class GameObject;

enum WeatherType
{
	Clear,
	Overcast,
	Stormy
};

class SkyboxObject : public GameObject
{
public:
	SkyboxObject();
	HRESULT Initialise(WeatherType type, DirectX::XMFLOAT3 translation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT3 rotation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), float scale = 1.0f);
	void SetShadersAndBuffers();
	void Render();
	void WriteDataToFile(std::string fileName);
	void CleanUp();
private:
	ID3D11ShaderResourceView* cubeMap = NULL;
	std::vector<Vertex> vertices;
};

#endif