#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <vector>
#include <Utils\DataStructures.h>
#include <Utils\Buffers\BufferFactory.h>
#include <Utils\FileLoading\FileLoader.h>
#include <Utils\ShaderFactory.h>
#include <Utils\Stopwatch.h>

enum Location
{
	CPU,
	CPU_MULTITHREADED,
	GPU
};

class GameObject
{
public:
	GameObject();
	virtual HRESULT Initialise(std::string name, DirectX::XMFLOAT3 translation, DirectX::XMFLOAT3 rotation, float scale, const std::string& shaderFile);

	virtual void CleanUp();
	virtual void SetShadersAndBuffers();
	virtual void Render(){}
	virtual void Update(float previousFrameDuration);
	virtual void WriteDataToFile(std::string fileName);
	void ToggleWireframe();
	void ToggleAntialiasing();
	void SetWorldConstBuffer();
	bool exists(const char *fileName);

protected:
	std::vector<unsigned int> indices;
	std::wstring shaderFileName;

	DirectX::XMFLOAT3		translation, rotation, scale;

	const std::string dt = "Models/Default/COLOR.png";
	const std::string dn = "Models/Default/NRM.png";
	const std::string ds = "Models/Default/SPEC.png";

	D3D11_RASTERIZER_DESC	rasterizerDesc;
	ID3D11RasterizerState*	rasterizer = NULL;

	ID3D11Buffer*           vertexBuffer = NULL;
	D3D11_INPUT_ELEMENT_DESC* layout; UINT arraySize;
	ID3D11Buffer*			indexBuffer = NULL;

	ID3D11VertexShader*		vertexShader = NULL;
	ID3D11InputLayout*		inputLayout = NULL;
	ID3D11PixelShader*		pixelShader = NULL;

	Stopwatch*				stopWatch = NULL;
	std::string				name;
	Location				updateLocation;
};

#endif