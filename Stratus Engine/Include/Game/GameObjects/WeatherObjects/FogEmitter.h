#ifndef FOGEMITTER_H
#define FOGEMITTER_H

#include <Utils\ThreadPool.h>
#include <boost\thread\thread.hpp>
#include <DirectXMath.h>
#include <d3d11.h>
#include <Utils\DataStructures.h>
#include <Game\GameObjects\BaseObjects\GameObject.h>

class D3D11Graphics;
class GameObject;
class ComputeOutputBuffer;
class ComputeInputBuffer;


enum Density
{
	Low,
	Average,
	High
};

//--------------------------------------------------------------------------------------
// Particle data structure
//--------------------------------------------------------------------------------------
struct FogParticle
{
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT3 Direction;
	float Speed;
	
	FogParticle(DirectX::XMFLOAT3 minBound, DirectX::XMFLOAT3 maxBound)
	{
		Position = DirectX::XMFLOAT3((maxBound.x + minBound.x) / 2.0f, randomFloat(minBound.y, maxBound.y), (maxBound.z + minBound.z) / 2.0f);
		Direction = NormalizeXMFLOAT3(DirectX::XMFLOAT3(randomFloat(-1.0f, 1.0f), 0.0f, randomFloat(-1.0f, 1.0f)));
		Speed = randomFloat(0.1f, 1.0f);
	}

	void Reset(DirectX::XMFLOAT3 minBound, DirectX::XMFLOAT3 maxBound)
	{
		int spawn = rand() % 2;
		if (spawn)
		{
			Position = DirectX::XMFLOAT3((maxBound.x + minBound.x) / 2.0f, randomFloat(minBound.y, maxBound.y), (maxBound.z + minBound.z) / 2.0f);
			Direction = NormalizeXMFLOAT3(DirectX::XMFLOAT3(randomFloat(-1.0f, 1.0f), 0.0f, randomFloat(-1.0f, 1.0f)));
			Speed = randomFloat(0.1f, 1.0f);
		}
	}

	void Update(float previousFrameDuration, DirectX::XMFLOAT3 minBound, DirectX::XMFLOAT3 maxBound)
	{
		Position += Direction * previousFrameDuration * Speed;
	}
};

//--------------------------------------------------------------------------------------
// Particle manager class
//--------------------------------------------------------------------------------------
class FogEmitter : public GameObject
{
public:
	FogEmitter();
	HRESULT Initialise(Location updateLocation, std::string name, Density density, DirectX::XMFLOAT3 minBound, DirectX::XMFLOAT3 maxBound,
		DirectX::XMFLOAT3 translation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT3 rotation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), float scale = 1.0f);
	void CreateParticles();
	void Update(float previousFrameDuration);
	void SetShadersAndBuffers();
	void Render();
	void WriteDataToFile(std::string fileName);
	void CleanUp();

private:
	ID3D11ShaderResourceView* texture = NULL;
	ID3D11GeometryShader*	geometryShader = NULL;
	ID3D11ComputeShader* computeShader = NULL;

	ComputeInputBuffer* computeInputBuffer = NULL;
	ComputeOutputBuffer* computeOutputBuffer = NULL;
	ID3D11Buffer* batchSizeBuffer = NULL;
	DirectX::XMINT4 batchSize;

	ThreadPool* threadPool;

	std::vector<FogParticle> vertices;

	int particleLimit;

	DirectX::XMFLOAT3 minBound;
	DirectX::XMFLOAT3 maxBound;

	void UpdateOnCPU(float previousFrameDuration);
	void UpdateOnCPUMultithreaded(float previousFrameDuration);
	void UpdateOnGPU(float previousFrameDuration);
};

#endif