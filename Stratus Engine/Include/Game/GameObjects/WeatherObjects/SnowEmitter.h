#ifndef SNOWEMITTER_H
#define SNOWEMITTER_H

#include <Utils\ThreadPool.h>
#include <boost\thread\thread.hpp>
#include <DirectXMath.h>
#include <d3d11.h>
#include <Utils\DataStructures.h>
#include <Game\GameObjects\BaseObjects\GameObject.h>
#include <Utils\NormalDistribution.h>

const float snowCrystalMass = 0.000000029f; //Water molecule = 2.992*10^-26kg, water molecules per snow crystal = 10^18, snow crystal = 2.9*10^-8kg

class D3D11Graphics;
class ComputeOutputBuffer;
class ComputeInputBuffer;
class ParticleCloudObject;

//--------------------------------------------------------------------------------------
// Particle data structure
//--------------------------------------------------------------------------------------
struct SnowParticle
{
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT3 Direction;
	int texID;
	float Radius;
	float Velocity;
	float TerminalVelocity;
	float Mass;

	SnowParticle()
	{
		Position = DirectX::XMFLOAT3(0.0f, -1.0f, 0.0f);
		Direction = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
		int texID = 0;
		float Radius = 0.0f;
		float Velocity = 0.0f;
		float TerminalVelocity = 0.0f;
		float Mass = 0.0f;
	}

	void Reset(DirectX::XMFLOAT3 minBound, DirectX::XMFLOAT3 maxBound)
	{
		int spawn = rand() % 2;
		if (spawn)
		{
			const float minRad = 0.001f;
			const float maxRad = 0.025f;
			const float dragCoefficient = 0.04f;

			this->Position = DirectX::XMFLOAT3(
				NormalDistribution::GetRandomValue(minBound.x, maxBound.x), 
				NormalDistribution::GetRandomValue(minBound.y, maxBound.y), 
				NormalDistribution::GetRandomValue(minBound.z, maxBound.z));
			this->Direction = DirectX::XMFLOAT3(randomFloat(0.0f, 1.0f), 0.0f, randomFloat(0.0f, 1.0f));
			this->texID = rand() % 6 + 1;
			this->Radius = randomFloat(minRad, maxRad);
			this->Velocity = 0.0f;
			Mass = snowCrystalMass * (rand() % 150 + 50); // Average crystals per snowflake = 100, allowed 50 crystals either side for best results
			this->TerminalVelocity = (-1.0f * sqrt((2.0f*Mass*9.81f) / (1.2f * Radius * dragCoefficient)));
		}
	}

	void Update(float previousFrameDuration)
	{
		if (Velocity > TerminalVelocity/10.0f) //Terminal Velocity divided by 10 due to lack of realism in game environment
			Velocity += previousFrameDuration * (Mass * -9.81f * 10000.0f); //acceleration due to gravity multiplied by 10,000 due to conversion from cm to m in radius
		Position.y += Velocity;
	}
};

//--------------------------------------------------------------------------------------
// Particle manager class
//--------------------------------------------------------------------------------------
class SnowEmitter : public GameObject
{
public:
	SnowEmitter();
	HRESULT Initialise(Location updateLocation, std::string name, ParticleCloudObject* cloud, FallType fallType,
		DirectX::XMFLOAT3 translation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT3 rotation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), float scale = 1.0f);
	void CreateParticles();
	HRESULT LoadTextures();
	void Update(float previousFrameDuration);
	void SetShadersAndBuffers();
	void Render();
	void WriteDataToFile(std::string fileName);
	void CleanUp();

private:
	std::vector<ID3D11ShaderResourceView*> textures;
	ID3D11GeometryShader*	geometryShader = NULL;
	ID3D11ComputeShader* computeShader = NULL;

	ThreadPool* threadPool;

	ComputeInputBuffer* computeInputBuffer = NULL;
	ComputeOutputBuffer* computeOutputBuffer = NULL;
	ID3D11Buffer* batchSizeBuffer = NULL;
	DirectX::XMINT4 batchSize;

	std::vector<SnowParticle> vertices;

	ParticleCloudObject* parentCloud;

	int particleLimit;

	void UpdateOnCPU(float previousFrameDuration);
	void UpdateOnCPUMultithreaded(float previousFrameDuration);
	void UpdateOnGPU(float previousFrameDuration);
};

#endif