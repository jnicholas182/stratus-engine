#ifndef RAINEMITTER_H
#define RAINEMITTER_H

#include <Utils\ThreadPool.h>
#include <boost\thread\thread.hpp>
#include <DirectXMath.h>
#include <d3d11.h>
#include <Utils\DataStructures.h>
#include <Game\GameObjects\BaseObjects\GameObject.h>
#include <Utils\NormalDistribution.h>

class D3D11Graphics;
class GameObject;
class ComputeOutputBuffer;
class ComputeInputBuffer;
class ParticleCloudObject;

//--------------------------------------------------------------------------------------
// Particle data structure
//--------------------------------------------------------------------------------------
struct RainParticle
{
	DirectX::XMFLOAT3 Position;
	float Radius;
	float Velocity;
	float TerminalVelocity;
	float Mass;

	RainParticle()
	{
		Position = DirectX::XMFLOAT3(0.0f, -1.0f, 0.0f);
		Radius = 0.0f;
		Velocity = 0.0f;
		TerminalVelocity = 0.0f;
		Mass = 0.0f;
	}

	void Reset(DirectX::XMFLOAT3 minBound, DirectX::XMFLOAT3 maxBound)
	{
		int spawn = rand() % 2;
		if (spawn)
		{
			const float minRad = 0.0010f;// 0.0005f; //Changed from 0.0005 to 0.001 due to some particles terminal velocity resulting in falling too slow
			const float maxRad = 0.0026f;
			const float dragCoefficient = 0.47f;

			this->Position = DirectX::XMFLOAT3(
				NormalDistribution::GetRandomValue(minBound.x, maxBound.x),
				NormalDistribution::GetRandomValue(minBound.y, maxBound.y),
				NormalDistribution::GetRandomValue(minBound.z, maxBound.z));
			this->Radius = randomFloat(minRad, maxRad);
			this->Velocity = 0.0f;
			float volume = (4.0f / 3.0f)*(DirectX::XM_PI)*(Radius*Radius*Radius);
			Mass = volume * 999.97f;
			this->TerminalVelocity = -1.0f * sqrt((2.0f*Mass*9.81f) / (1.2f * (DirectX::XM_PI * Radius * Radius) * dragCoefficient));
		}
	}

	void Update(float previousFrameDuration)
	{
		if (Velocity > TerminalVelocity/10.0f) //Terminal Velocity divided by 10 due to lack of realism in game environment
			Velocity += previousFrameDuration * (Mass * (-9.81f * 10000.0f)); //acceleration due to gravity multiplied by 10,000 due to conversion from cm to m in radius
		Position.y += Velocity;
	}
};

//--------------------------------------------------------------------------------------
// Particle manager class
//--------------------------------------------------------------------------------------
class RainEmitter : public GameObject
{
public:
	RainEmitter();
	HRESULT Initialise(Location updateLocation, std::string name, ParticleCloudObject* cloud, FallType fallType,
	DirectX::XMFLOAT3 translation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT3 rotation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), float scale = 1.0f);
	void CreateParticles();
	void Update(float previousFrameDuration);
	void SetShadersAndBuffers();
	void Render();
	void WriteDataToFile(std::string fileName);
	void CleanUp();

private:
	ID3D11ShaderResourceView* texture = NULL;
	ID3D11GeometryShader*	geometryShader = NULL;
	ID3D11ComputeShader* computeShader = NULL;

	ComputeInputBuffer* computeInputBuffer = NULL;
	ComputeOutputBuffer* computeOutputBuffer = NULL;
	ID3D11Buffer* batchSizeBuffer = NULL;
	DirectX::XMINT4 batchSize;

	ThreadPool* threadPool;

	std::vector<RainParticle> vertices;

	ParticleCloudObject* parentCloud;

	int particleLimit;

	void UpdateOnCPU(float previousFrameDuration);
	void UpdateOnCPUMultithreaded(float previousFrameDuration);
	void UpdateOnGPU(float previousFrameDuration);
};

#endif