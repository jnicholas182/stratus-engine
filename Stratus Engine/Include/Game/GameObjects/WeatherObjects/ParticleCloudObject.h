#ifndef PARTICLECLOUDOBJECT_H
#define PARTICLECLOUDOBJECT_H

#include <Utils\ThreadPool.h>
#include <boost\thread\thread.hpp>
#include <DirectXMath.h>
#include <d3d11.h>
#include <Utils\DataStructures.h>
#include <Game\GameObjects\BaseObjects\GameObject.h>
#include <Utils\NormalDistribution.h>

class D3D11Graphics;
class GameObject;
class ComputeOutputBuffer;
class ComputeInputBuffer;

enum Size
{
	Small,
	Medium,
	Large
};

//--------------------------------------------------------------------------------------
// Particle data structure
//--------------------------------------------------------------------------------------
struct CloudParticle
{
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT2 Velocity;

	CloudParticle(DirectX::XMFLOAT3 minBound, DirectX::XMFLOAT3 maxBound)
	{
		Position = DirectX::XMFLOAT3(
			NormalDistribution::GetRandomValue(minBound.x, maxBound.x),
			NormalDistribution::GetRandomValue(minBound.y, maxBound.y),
			NormalDistribution::GetRandomValue(minBound.z, maxBound.z)
		);
		Velocity = DirectX::XMFLOAT2(0.0f, 0.0f);
	}
};

//--------------------------------------------------------------------------------------
// Particle manager class
//--------------------------------------------------------------------------------------
class ParticleCloudObject : public GameObject
{
public:
	ParticleCloudObject();
	HRESULT Initialise(Location updateLocation, std::string name, Size size, DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 translation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT3 rotation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), float scale = 1.0f);
	void CreateParticles();
	void Update(float previousFrameDuration);
	void UpdateCloudSize();
	void SetShadersAndBuffers();
	void Render();
	void WriteDataToFile(std::string fileName);
	void CleanUp();

	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 minBound;
	DirectX::XMFLOAT3 maxBound;
private:
	ID3D11ShaderResourceView* texture = NULL;
	ID3D11GeometryShader*	geometryShader = NULL;
	ID3D11ComputeShader* computeShader = NULL;

	ComputeInputBuffer* computeInputBuffer = NULL;
	ComputeOutputBuffer* computeOutputBuffer = NULL;
	ID3D11Buffer* batchSizeBuffer = NULL;
	DirectX::XMINT4 batchSize;
	ID3D11Buffer* cloudCentreBuffer = NULL;

	ThreadPool* threadPool;

	std::vector<CloudParticle> vertices;
	float xSize, ySize, zSize;

	int particleLimit;

	void UpdateOnCPU(float previousFrameDuration);
	void UpdateOnCPUMultithreaded(float previousFrameDuration);
	void ThreadedUpdate(float previousFrameDuration, int index);
	void UpdateOnGPU(float previousFrameDuration);

	DirectX::XMFLOAT2 getSeparation(int index);
};

#endif