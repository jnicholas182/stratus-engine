#ifndef LIGHTNINGOBJECT_H
#define LIGHTNINGOBJECT_H

#include <Utils\ThreadPool.h>
#include <boost\thread\thread.hpp>
#include <DirectXMath.h>
#include <d3d11.h>
#include <Utils\DataStructures.h>
#include <Game\GameObjects\BaseObjects\GameObject.h>
#include <Utils\NormalDistribution.h>

class D3D11Graphics;
class GameObject;
class ComputeOutputBuffer;
class ComputeInputBuffer;
class ParticleCloudObject;

//--------------------------------------------------------------------------------------
// Point data structure
//--------------------------------------------------------------------------------------
struct LightningSegment
{
	DirectX::XMFLOAT3 StartPoint;
	DirectX::XMFLOAT3 EndPoint;

	LightningSegment(DirectX::XMFLOAT3 StartPoint, DirectX::XMFLOAT3 EndPoint)
	{
		this->StartPoint = StartPoint;
		this->EndPoint = EndPoint;
	}
};

//--------------------------------------------------------------------------------------
// Lightning bolt class
//--------------------------------------------------------------------------------------
class LightningObject : public GameObject
{
public:
	LightningObject();
	HRESULT Initialise(std::string name, ParticleCloudObject* parentCloud, DirectX::XMFLOAT3 translation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT3 rotation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), float scale = 1.0f);
	void Update(float previousFrameDuration);
	void SetShadersAndBuffers();
	void Render();
	void WriteDataToFile(std::string fileName);
	void CleanUp();

private:
	std::vector<DirectX::XMFLOAT3> vertices;
	ParticleCloudObject* parentCloud;
};

#endif