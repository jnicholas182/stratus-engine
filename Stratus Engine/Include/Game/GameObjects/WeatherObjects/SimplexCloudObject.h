#ifndef PERLINCLOUDOBJECT_H
#define PERLINCLOUDOBJECT_H

#include <Utils\ThreadPool.h>
#include <boost\thread\thread.hpp>

#include <Game\GameObjects\BaseObjects\GameObject.h>

class SimplexNoise;
class ComputeShader;
class ComputeOutputBuffer;

struct PermutationTableStruct
{
	int permutationTable[512];
};

struct PerlinCloudData
{
	float yAnimationVal;
	float zAnimationVal;
	float cover;
	float sharpness;
};

enum CloudCover
{
	Cloud_Clear,
	Cloud_Slight,
	Cloud_Overcast,
	Cloud_Thick
};

class SimplexCloudObject : public GameObject
{
public:
	SimplexCloudObject();
	HRESULT Initialise(Location updateLocation, std::string name, CloudCover coverType, float altitude,
	DirectX::XMFLOAT3 translation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT3 rotation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), float scale = 1.0f);
	void SetShadersAndBuffers();
	void Render();
	void Update(float previousFrameDuration);
	void CreateMesh(float size, float altitude);
	HRESULT CreateTexture();
	void WriteDataToFile(std::string fileName);
	void CleanUp();
	

private:
	ID3D11ShaderResourceView* texture = NULL;
	ID3D11Texture2D *tex = NULL;
	int width, height;
	SimplexNoise *noiseGenerator;
	PerlinCloudData cloudData;
	float yAnimationSpeed;
	float zAnimationSpeed;
	UINT32 *textureData;

	std::vector<Vertex> vertices;

	ThreadPool* threadPool;
	void getPerlinValue(int x, int y);
	void smoothTexture();

	ID3D11ComputeShader* computeShader = NULL;
	ComputeOutputBuffer* computeOutputBuffer = NULL;
	ID3D11Buffer* batchSizeBuffer = NULL;
	DirectX::XMINT4 batchSize;
	ID3D11Buffer* permutationTableBuffer = NULL;
	ID3D11Buffer* cloudDataBuffer = NULL;

	void UpdateOnCPU(float previousFrameDuration);
	void UpdateOnCPUMultithreaded(float previousFrameDuration);
	void UpdateOnGPU(float previousFrameDuration);
};

#endif