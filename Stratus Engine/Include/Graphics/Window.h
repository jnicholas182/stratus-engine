#ifndef WINAPP_H
#define WINAPP_H

#include <Windows.h>
#include <Utils\resource.h>

class Window
{
public:
	HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	HINSTANCE               g_hInst = NULL;
	HWND                    g_hWnd = NULL;
};
#endif