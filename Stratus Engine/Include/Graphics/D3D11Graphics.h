#ifndef D3D11GRAPHICS_H
#define D3D11GRAPHICS_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <directxcolors.h>
#include <Utils\resource.h>
#include <Utils\Buffers\BufferFactory.h>

#include <vector>

class D3D11Graphics{
public:
	static D3D11Graphics& getInstance()
	{
		static D3D11Graphics instance;
		return instance;
	}

	HRESULT InitDevice(HWND g_hWnd);

	HRESULT CreateSwapChain(HWND g_hWnd, UINT width, UINT height);
	HRESULT CreateRenderTargetView();

	HRESULT CreateDepthStencilTextureResource(UINT width, UINT height);
	HRESULT CreateDepthStencilState();
	HRESULT CreateDepthStencilView();

	void SetupViewport(UINT width, UINT height);
	HRESULT CreateSampleState();
	HRESULT CreateBlendState();

	void ClearBackBuffer();
	void ClearDepthStencil();
	void PresentSwapChain();
	void CleanUp();
	ID3D11Device* getDevice() const;
	ID3D11DeviceContext* getContext() const;
	ID3D11Buffer* getWorldBuffer() const;
	void SetSamplerLinear();

	UINT width, height;

private:
	D3D11Graphics(){};
	D3D11Graphics(D3D11Graphics const&) = delete;
	void operator=(D3D11Graphics const&) = delete;

	D3D_DRIVER_TYPE         g_driverType = D3D_DRIVER_TYPE_NULL;
	D3D_FEATURE_LEVEL       g_featureLevel = D3D_FEATURE_LEVEL_11_0;
	ID3D11Device*           g_pd3dDevice = NULL;
	ID3D11DeviceContext*    g_pImmediateContext = NULL;
	IDXGISwapChain*         g_pSwapChain = NULL;
	ID3D11RenderTargetView* g_pRenderTargetView = NULL;

	ID3D11BlendState* Transparency = NULL;

	ID3D11Texture2D*			DepthStencil = NULL;
	ID3D11DepthStencilState*	DepthStencilState = NULL;
	ID3D11DepthStencilView*		DepthStencilView = NULL;
	ID3D11SamplerState*			SamplerLinear = NULL;
	D3D11_VIEWPORT				defaultViewport;

	ID3D11Buffer*			WorldBuffer = NULL;
	ID3D11Buffer*			ProjectionBuffer = NULL;
};
#endif