#ifndef CAMERA_H
#define CAMERA_H

#include <Windows.h>
#include <new>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <Utils\resource.h>
#include <Utils\DataStructures.h>
#include <vector>

class GameObject;
class TerrainObject;

class Camera
{
public:
	static Camera& getInstance()
	{
		static Camera instance;
		return instance;
	}
	HRESULT Initialise(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 direction);
	void Update(float previousFrameDuration);
	void CleanUp();
	CameraData* GetCameraData();

	void * operator new (size_t size)
	{
		void* p = _aligned_malloc(size, 16);
		if (p == 0) throw std::bad_alloc();
		return p;
	}
	void operator delete(void *p)
	{
		_aligned_free(p);
	}

private:
	Camera(){};
	Camera(Camera const&) = delete;
	void operator=(Camera const&) = delete;

	void ApplyRotation(CameraData& cameraData);
	CameraData* cameraData;
	ID3D11Buffer* ViewBuffer = NULL;
	ID3D11Buffer* CameraDataBuffer = NULL;
};

#endif