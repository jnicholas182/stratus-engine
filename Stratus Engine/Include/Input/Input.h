#ifndef INPUT_H
#define INPUT_H

#define KEY_DOWN(virt_key_code)  ( (GetAsyncKeyState(virt_key_code) & 0x8000) ? 1 : 0 )
#define KEY_TOGGLED(virt_key_code)  ( (GetAsyncKeyState(virt_key_code) & 1) ? 1 : 0 )

#include <Windows.h>
#include <vector>
#include <Utils\DataStructures.h>

class Gamepad;
class GameObject;
class SkyboxObject;

class Input
{
public:
	static void ReceiveKeyboardInput(float previousFrameDuration, HWND g_hWnd, std::vector<GameObject*> &gameObjects, SkyboxObject* skybox);
	static void ReceiveControllerInput(float previousFrameDuration, HWND g_hWnd, Gamepad* gamepad, std::vector<GameObject*> &gameObjects, SkyboxObject* skybox);
};

#endif