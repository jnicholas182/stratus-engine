#ifndef GAMEPAD_H
#define GAMEPAD_H

#include <Xinput.h>

// XInput Button values
static const WORD XINPUT_Buttons[] = {
	XINPUT_GAMEPAD_A,
	XINPUT_GAMEPAD_B,
	XINPUT_GAMEPAD_X,
	XINPUT_GAMEPAD_Y,
	XINPUT_GAMEPAD_DPAD_UP,
	XINPUT_GAMEPAD_DPAD_DOWN,
	XINPUT_GAMEPAD_DPAD_LEFT,
	XINPUT_GAMEPAD_DPAD_RIGHT,
	XINPUT_GAMEPAD_LEFT_SHOULDER,
	XINPUT_GAMEPAD_RIGHT_SHOULDER,
	XINPUT_GAMEPAD_LEFT_THUMB,
	XINPUT_GAMEPAD_RIGHT_THUMB,
	XINPUT_GAMEPAD_START,
	XINPUT_GAMEPAD_BACK
};

// XInput Button IDs
struct XButtonIDs
{
	XButtonIDs();

	int A, B, X, Y;
	int DPad_Up, DPad_Down, DPad_Left, DPad_Right;
	int L_Shoulder, R_Shoulder;
	int L_Thumbstick, R_Thumbstick;
	int Start;
	int Back;
};

class Gamepad
{
public:
	Gamepad(int a_iIndex);

	void Update();

	XINPUT_STATE GetState();
	XINPUT_STATE GetPrevState();

	int GetIndex();
	bool Connected();

	bool LStick_InDeadzone();
	bool RStick_InDeadzone();

	float LeftStick_X();
	float LeftStick_Y();
	float RightStick_X();
	float RightStick_Y();

	float LeftTrigger();
	float RightTrigger();

	bool GetButtonPressed(int a_iButton);
	bool GetButtonDown(int a_iButton);
	bool GetButtonReleased(int a_iButton);

private:
	XINPUT_STATE m_State, prevState;
	int m_iGamepadIndex;

	static const int ButtonCount = 14;
	bool bPrev_ButtonStates[ButtonCount];
	bool bButtonStates[ButtonCount];

	bool bGamepad_ButtonsDown[ButtonCount];
};

extern XButtonIDs XButtons;

#endif