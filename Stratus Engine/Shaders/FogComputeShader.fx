#include "ShaderResource.hlsli"

struct FogParticle
{
	float3 Position;
	float3 Direction;
	float Speed;
};

StructuredBuffer<FogParticle> BufferIn : register(t10); //10 due to other registers taken by textures
RWStructuredBuffer<FogParticle> BufferOut : register(u0);

#define numthreads_x 5
#define numthreads_y 5
#define numthreads_z 4

[numthreads(numthreads_x, numthreads_y, numthreads_z)]
void main(uint3 groupThreadID : SV_GroupThreadID, uint3 groupID : SV_GroupID)
{
	uint index = CalcThreadIndex(groupThreadID, groupID, uint3(numthreads_x, numthreads_y, numthreads_z));
	FogParticle returnParticle = BufferIn[index];
	float movement = previousFrameDuration * returnParticle.Speed;
	returnParticle.Position += returnParticle.Direction * movement;
	BufferOut[index] = returnParticle;
}