#include "ShaderResource.hlsli"

struct CloudParticle
{
	float3 Position;
	float2 Velocity;
};

StructuredBuffer<CloudParticle> BufferIn : register(t11); //10 due to other registers taken by textures
RWStructuredBuffer<CloudParticle> BufferOut : register(u0);

#define numthreads_x 5
#define numthreads_y 5
#define numthreads_z 4

float2 getSeparation(unsigned int index, CloudParticle returnParticle)
{
	float2 averagePos = float2(0.0f, 0.0f);
	unsigned int neighbourCount = 0;
	unsigned int count = (numthreads_x * numthreads_y * numthreads_z) * (batchsize.x * batchsize.y * batchsize.z);
	for (unsigned int i = 0; i < count; i++)
	{
		if (i != index)
		{
			averagePos.x += BufferIn[i].Position.x - returnParticle.Position.x;
			averagePos.y += BufferIn[i].Position.z - returnParticle.Position.z;
		}
	}
	averagePos.x /= count; averagePos.y /= count;
	averagePos.x *= -1.0f; averagePos.y *= -1.0f;
	return normalize(averagePos);
}

[numthreads(numthreads_x, numthreads_y, numthreads_z)]
void main(uint3 groupThreadID : SV_GroupThreadID, uint3 groupID : SV_GroupID)
{
	uint index = CalcThreadIndex(groupThreadID, groupID, uint3(numthreads_x, numthreads_y, numthreads_z));
	CloudParticle returnParticle = BufferIn[index];

	float speed = previousFrameDuration * 0.001f;
	float2 cohesion = normalize(float2(cloudCentre.x - returnParticle.Position.x, cloudCentre.z - returnParticle.Position.z)); //Aim for centre of cloud
	float2 separation = getSeparation(index, returnParticle); //Separate from other particles
	returnParticle.Velocity = returnParticle.Velocity + speed * normalize(float2(cohesion.x * 0.6f + (separation.x * 0.3f), cohesion.y * 0.6f + (separation.y * 0.3f)));
	returnParticle.Position.x += returnParticle.Velocity.x; returnParticle.Position.z += returnParticle.Velocity.y;
	
	BufferOut[index] = returnParticle;
}

