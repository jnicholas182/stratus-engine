//--------------------------------------------------------------------------------------
// Shader to be used by billboarded objects
//--------------------------------------------------------------------------------------

#include "ShaderResource.hlsli"

struct BillboardVS_Out
{
	float4 position : SV_POSITION;
	float4 direction : DIRECTON;
	int texID : TEXID;
	float4 world : POSITION;
	float4 normal : NORMAL;
	float2 texCoord : TEXCOORD;
};

struct BillboardGS_Out
{
	float4 position : SV_POSITION;
	int texID : TEXID;
	float4 world : POSITION;
	float4 normal : NORMAL;
	float2 texCoord : TEXCOORD;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
BillboardVS_Out VS(float4 position : POSITION, float4 direction : DIRECTION, int texID : TEXID)
{
	BillboardVS_Out output;
	output.position = mul(Projection, mul(View, mul(World, position)));
	output.direction = direction;
	output.texID = texID;
	output.world = mul(World, position);
	return output;
}

//--------------------------------------------------------------------------------------
// Geometry Shader
//--------------------------------------------------------------------------------------
[maxvertexcount(4)]
void GS(point BillboardVS_Out input[1], inout TriangleStream< BillboardGS_Out > output)
{
	float3 planeNormal = normalize(input[0].direction.xyz);

	float3 upVector = float3(0.0f, 1.0f, 0.0f);
	float3 rightVector = normalize(cross(planeNormal, upVector));
	rightVector = rightVector * (0.1f / 2.0f);
	upVector = float3(0.0f, 0.1f, 0.0f);

	float3 vert[4];
	vert[1] = input[0].world.xyz + rightVector; // Get bottom right vertex
	vert[0] = input[0].world.xyz - rightVector; // Get bottom left vertex
	vert[2] = input[0].world.xyz - rightVector + upVector; // Get top left vertex
	vert[3] = input[0].world.xyz + rightVector + upVector; // Get top right vertex

	// Get billboards texture coordinates
	float2 texCoord[4];
	texCoord[0] = float2(0, 1);
	texCoord[1] = float2(1, 1);
	texCoord[2] = float2(0, 0);
	texCoord[3] = float2(1, 0);

	BillboardGS_Out outputVert;
	for (int i = 0; i < 4; i++)
	{
		outputVert.position = mul(Projection, mul(View, mul(World, float4(vert[i], 1.0f))));
		outputVert.texID = input[0].texID;
		outputVert.world = float4(vert[i], 1.0f);
		outputVert.texCoord = texCoord[i];
		outputVert.normal = float4(planeNormal, 1.0f);

		output.Append(outputVert);
	}
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(BillboardGS_Out input) : SV_TARGET
{
	if (input.texID == 1)
		return float4(snowflake1.Sample(samLinear, input.texCoord).rgba);
	else if (input.texID == 2)
		return float4(snowflake2.Sample(samLinear, input.texCoord).rgba);
	else if (input.texID == 3)
		return float4(snowflake3.Sample(samLinear, input.texCoord).rgba);
	else if (input.texID == 4)
		return float4(snowflake4.Sample(samLinear, input.texCoord).rgba);
	else if (input.texID == 5)
		return float4(snowflake5.Sample(samLinear, input.texCoord).rgba);
	else if (input.texID == 6)
		return float4(snowflake6.Sample(samLinear, input.texCoord).rgba);

	return float4(textureDiffuse.Sample(samLinear, input.texCoord).rgba);
}