#include "ShaderResource.hlsli"

RWStructuredBuffer<int> BufferOut : register(u0);

#define numthreads_x 16
#define numthreads_y 16
#define numthreads_z 4

double fade(double t)
{
	return t * t * t * (t * (t * 6 - 15) + 10);
}

double lerp(double t, double a, double b)
{
	return a + t * (b - a);
}

double grad(uint hash, double x, double y, double z)
{
	uint h = hash & 15;
	// Convert lower 4 bits of hash into 12 gradient directions
	double u = h < 8 ? x : y, v = h < 4 ? y : h == 12 || h == 14 ? x : z;
	return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}

double raw3DNoise(double x, double y, double z) 
{
	// Find unit cube containing point x, y, z
	uint X = (uint)floor(x) & 255;
	uint Y = (uint)floor(y) & 255;
	uint Z = (uint)floor(z) & 255;

	// Find relative x, y, z of point in unit cube
	x -= floor(x);
	y -= floor(y);
	z -= floor(z);

	// Compute fade curves
	double u = fade(x);
	double v = fade(y);
	double w = fade(z);

	// Hash coordinates of unit cube corners
	uint A = permutation[X / 4][X % 4] + Y;
	uint AA = permutation[A / 4][A % 4] + Z;
	uint AB = permutation[(A + 1) / 4][(A + 1) % 4] + Z;
	uint B = permutation[(X + 1) / 4][(X + 1) % 4] + Y;
	uint BA = permutation[B / 4][B % 4] + Z;
	uint BB = permutation[(B + 1) / 4][(B + 1) % 4] + Z;

	// Add blended results from corners of unit cube
	double res = lerp(w, lerp(v, lerp(u, grad(permutation[AA / 4][AA % 4], x, y, z), 
										 grad(permutation[BA / 4][BA % 4], x - 1, y, z)), 
								 lerp(u, grad(permutation[AB / 4][AB % 4], x, y - 1, z), 
										 grad(permutation[BB / 4][BB % 4], x - 1, y - 1, z))),
						 lerp(v, lerp(u, grad(permutation[(AA + 1)/4][(AA + 1)%4], x, y, z - 1), 
										 grad(permutation[(BA + 1)/4][(BA + 1)%4], x - 1, y, z - 1)), 
								 lerp(u, grad(permutation[(AB + 1)/4][(AB + 1)%4], x, y - 1, z - 1), 
										 grad(permutation[(BB + 1)/4][(BB + 1)%4], x - 1, y - 1, z - 1))));
	// Return value between 0 and 1
	return (res + 1.0) / 2.0;
}

double getNoiseValue(int octaves, float persistence, float scale, double x, double y, double z)
{
	double total = 0;
	float frequency = scale;
	float amplitude = 1;
	float maxAmplitude = 0;

	for (int i = 0; i < octaves; i++) {
		total += raw3DNoise(x * frequency, y * frequency, z * frequency) * amplitude;

		frequency *= 2;
		maxAmplitude += amplitude;
		amplitude *= persistence;
	}

	return total / maxAmplitude;
}

uint exponentialFilter(uint perlinValue)
{
	float c = perlinValue - (255.0f - cover);
	if (c < 0.0f)
		c = 0.0f;

	return floor(255.0f - ((float)(pow(sharpness, c))*255.0f));
}

[numthreads(numthreads_x, numthreads_y, numthreads_z)]
void main(uint3 groupThreadID : SV_GroupThreadID, uint3 groupID : SV_GroupID)
{
	uint index = CalcThreadIndex(groupThreadID, groupID, uint3(numthreads_x, numthreads_y, numthreads_z));

	uint i_x = index % 256;
	uint i_y = index / 256;

	uint value = floor(255.0 * getNoiseValue(8, 0.5f, 1.0f, i_x*0.06, (i_y*0.06) + yAnimationVal, zAnimationVal));

	value = exponentialFilter(value);
	uint returnVal = value | value << 8 | value << 16 | value << 24; 	//rgba

	BufferOut[index] = returnVal;
}