//--------------------------------------------------------------------------------------
// Shader to be used to render a skybox
//--------------------------------------------------------------------------------------

#include "ShaderResource.hlsli"

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_In VS(float4 position : POSITION, float4 normal : NORMAL, float2 texCoord : TEXCOORD)
{
	PS_In output;

	output.position = mul(Projection, mul(View, mul(World, position))).xyzw;
	output.normal = normal;
	output.world = mul(World, position);

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_In input) : SV_TARGET
{
	return float4(textureCubeMap.Sample(CubeMapSampler, input.position.xyz).rgb, 1.0f);
}