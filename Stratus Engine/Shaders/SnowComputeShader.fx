#include "ShaderResource.hlsli"

struct SnowParticle
{
	float3 Position;
	float3 Direction;
	int texID;
	float Radius;
	float Velocity;
	float TerminalVelocity;
	float Mass;
};

StructuredBuffer<SnowParticle> BufferIn : register(t9); //9 due to other registers taken by textures
RWStructuredBuffer<SnowParticle> BufferOut : register(u0);

#define numthreads_x 5
#define numthreads_y 5
#define numthreads_z 4

[numthreads(numthreads_x, numthreads_y, numthreads_z)]
void main(uint3 groupThreadID : SV_GroupThreadID, uint3 groupID : SV_GroupID)
{
	uint index = CalcThreadIndex(groupThreadID, groupID, uint3(numthreads_x, numthreads_y, numthreads_z));
	SnowParticle returnParticle = BufferIn[index];

	if (returnParticle.Velocity > returnParticle.TerminalVelocity / 10.0f) //Terminal Velocity divided by 10 due to lack of realism in game environment
		returnParticle.Velocity += previousFrameDuration * (returnParticle.Mass * (-9.81f * 10000.0f)); //acceleration due to gravity multiplied by 10,000 due to conversion from cm to m in radius
	returnParticle.Position.y += returnParticle.Velocity;

	BufferOut[index] = returnParticle;
}