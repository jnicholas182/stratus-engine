//--------------------------------------------------------------------------------------
// Texture registers
//--------------------------------------------------------------------------------------
Texture2D textureDiffuse : register(t0);
TextureCube textureCubeMap : register(t1);

Texture2D snowflake1 : register(t2);
Texture2D snowflake2 : register(t3);
Texture2D snowflake3 : register(t4);
Texture2D snowflake4 : register(t5);
Texture2D snowflake5 : register(t6);
Texture2D snowflake6 : register(t7);

//--------------------------------------------------------------------------------------
// Texture sampler
//--------------------------------------------------------------------------------------
SamplerState samLinear : register(s0);

//--------------------------------------------------------------------------------------
// Cube map sampler
//--------------------------------------------------------------------------------------
SamplerState CubeMapSampler
{
	Filter = ANISOTROPIC;
	MaxAnisotropy = 4;

	AddressU = WRAP;
	AddressV = WRAP;
};

//--------------------------------------------------------------------------------------
// World / View / Projection buffers
//--------------------------------------------------------------------------------------
cbuffer WorldConstantBuffer : register(b0)
{
	matrix World;
}
cbuffer ViewConstantBuffer : register(b1)
{
	matrix View;
}
cbuffer ProjectionConstantBuffer : register(b2)
{
	matrix Projection;
}

//--------------------------------------------------------------------------------------
// Camera position buffer
//--------------------------------------------------------------------------------------
cbuffer CameraData : register(b3)
{
	float4 CameraPosition;
}

//--------------------------------------------------------------------------------------
// Light data buffer
//--------------------------------------------------------------------------------------
cbuffer LightData : register(b4)
{
	float4 Position[16];
	float4 Direction[16];
	float4 Diffuse[16];
	float4 Specular[16];
	int4 type[16];
	float4 cone[4];
	int lightCount;
};

//--------------------------------------------------------------------------------------
// Compute shader batch size buffer
//--------------------------------------------------------------------------------------
cbuffer BatchSize : register(b5)
{
	uint4 batchsize;
};

//--------------------------------------------------------------------------------------
// Permutation table for perlin noise
//--------------------------------------------------------------------------------------
cbuffer PermutationTable : register(b6)
{
	int4 permutation[128];
};

//--------------------------------------------------------------------------------------
// Animation value for perlin clouds
//--------------------------------------------------------------------------------------
cbuffer PerlinCloudData : register(b7)
{
	float yAnimationVal;
	float zAnimationVal;
	float cover;
	float sharpness;
};

//--------------------------------------------------------------------------------------
// Duration of previous frame, allows updates to happen depended on framerate
//--------------------------------------------------------------------------------------
cbuffer PreviousFrameDuration : register(b8)
{
	float4 previousFrameDuration;
};

//--------------------------------------------------------------------------------------
// Duration of previous frame, allows updates to happen depended on framerate
//--------------------------------------------------------------------------------------
cbuffer CloudCentre : register(b9)
{
	float4 cloudCentre;
};

//--------------------------------------------------------------------------------------
// Data structure written by vertex shader and read by pixel shader
//--------------------------------------------------------------------------------------
struct PS_In
{
	float4 position : SV_POSITION;
	float4 world : POSITION;
	float4 normal : NORMAL;
	float2 texCoord : TEXCOORD;
};

//--------------------------------------------------------------------------------------
// Calculate thread index for compute shader
//--------------------------------------------------------------------------------------
int CalcThreadIndex(uint3 groupThreadID, uint3 groupID, uint3 numThreads)
{
	int groupMax = numThreads.x * numThreads.y * numThreads.z;
	int subGroupID = (((batchsize.z * groupID.z) * batchsize.y) + (batchsize.x * groupID.y) + groupID.x) * groupMax;
	int subThreadID = ((numThreads.x * groupThreadID.x) * numThreads.z) + (groupThreadID.z * numThreads.y) + groupThreadID.y;
	return subGroupID + subThreadID;
}