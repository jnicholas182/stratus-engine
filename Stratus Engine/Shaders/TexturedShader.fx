//--------------------------------------------------------------------------------------
// Shader to be used by objects requiring basic texturing
//--------------------------------------------------------------------------------------

#include "ShaderResource.hlsli"

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_In VS(float4 position : POSITION, float4 normal : NORMAL, float2 texCoord : TEXCOORD)
{
	PS_In output;

	output.position = mul(Projection, mul(View, mul(World, position)));
	output.normal = normalize(mul(World, float4(normal.xyz, 0.0f)));
	output.world = mul(World, position);
	output.texCoord = texCoord;

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_In input) : SV_TARGET
{
	float4 finalColor;
	float3 view = normalize(CameraPosition.xyz - input.world.xyz);
	float3 Ambient = float3(0.0f, 0.0f, 0.0f);
	 
	for (int i = 0; i<lightCount; i++)
	{
		float3 lightDirection = normalize(input.world.xyz - Position[i].xyz);

		float diffuse = max(dot(-lightDirection, input.normal.xyz), 0);
		float3 reflection = normalize(reflect(lightDirection, input.normal.xyz).xyz);
		float specular = pow(max(dot(view, reflection), 0), 50);

		finalColor.rgb += Ambient;

		if (type[i].x == 1)
		{
			float3 pointColor = float3(0.0f, 0.0f, 0.0f);
			pointColor.rgb += diffuse * Diffuse[i].rgb;
			//pointColor.rgb += specular * Specular[i].rgb;
			//float mag = length(Position[i].xyz - position.xyz);
			//pointColor /= (0.0f + (0.5f * mag) + (0.0f * (mag*mag)));
			finalColor.rgb += pointColor;
		}
		else if (type[i].x == 2)
		{
			finalColor.rgb += saturate(dot(Direction[i].xyz, input.normal.xyz) *  Diffuse[i].rgb);
			//finalColor.rgb += specular * Specular[i].rgb;
		}
		else if (type[i].x == 3)
		{
			float angledot = dot(lightDirection, normalize(Direction[i].xyz));
			float cone_range = cone[i / 4][i % 4];
			float cone_threshold = 1.0 - cone_range;
			angledot = max((angledot - cone_threshold) / cone_range, 0);

			finalColor.rgb += angledot * diffuse * Diffuse[i].rgb;
			finalColor.rgb += angledot * specular * Specular[i].rgb;
		}
	}
	finalColor.rgb *= textureDiffuse.Sample(samLinear, input.texCoord).rgb;
	return float4(finalColor.rgb, 1.0f);
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
/*PS_OUT PS(PS_In input) : SV_TARGET
{
	PS_OUT output;
	output.position = float4(input.world.xyz, 1.0f);
	output.normal = float4(input.normal.xyz, 1.0f);
	output.color = float4(textureDiffuse.Sample(samLinear, input.texCoord).rgb, 1.0f);

	return output;
}*/