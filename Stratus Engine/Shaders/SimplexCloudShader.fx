//--------------------------------------------------------------------------------------
// Shader to be used by objects requiring basic texturing
//--------------------------------------------------------------------------------------

#include "ShaderResource.hlsli"

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_In VS(float4 position : POSITION, float4 normal : NORMAL, float2 texCoord : TEXCOORD)
{
	PS_In output;

	output.position = mul(Projection, mul(View, mul(World, position)));
	output.normal = normalize(mul(World, float4(normal.xyz, 0.0f)));
	output.world = mul(World, position);
	output.texCoord = texCoord;

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_In input) : SV_TARGET
{
	float2 XZCameraToPoint = input.world.xz - CameraPosition.xz;
	float magnitude = length(XZCameraToPoint);

	float4 output = textureDiffuse.Sample(samLinear, input.texCoord).rgba;

	if (magnitude > 200.0f)
		output.a = output.a * (1-((magnitude-200.0f)/800.0f));

	if (output.a < 0.0f)
		output.a = 0.0f;

	return output;
}