//--------------------------------------------------------------------------------------
// Shader to be used by rain particles
//--------------------------------------------------------------------------------------

#include "ShaderResource.hlsli"

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_In VS(float4 position : POSITION)
{
	PS_In output;
	output.position = mul(Projection, mul(View, mul(World, position)));
	output.world = mul(World, position);
	return output;
}

//--------------------------------------------------------------------------------------
// Geometry Shader
//--------------------------------------------------------------------------------------
[maxvertexcount(4)]
void GS(point PS_In input[1], inout TriangleStream< PS_In > output)
{
	float3 planeNormal = input[0].world.xyz - CameraPosition.xyz;
	planeNormal.y = 0.0f;
	planeNormal = normalize(planeNormal);

	float3 upVector = float3(0.0f, 1.0f, 0.0f);
	float3 rightVector = normalize(cross(planeNormal, upVector));
	rightVector = rightVector * (0.02f / 2.0f);
	upVector = float3(0.0f, 0.5f, 0.0f);

	float3 vert[4];
	vert[1] = input[0].world.xyz + rightVector; // Get bottom right vertex
	vert[0] = input[0].world.xyz - rightVector; // Get bottom left vertex
	vert[2] = input[0].world.xyz - rightVector + upVector; // Get top left vertex
	vert[3] = input[0].world.xyz + rightVector + upVector; // Get top right vertex

	// Get billboards texture coordinates
	float2 texCoord[4];
	texCoord[0] = float2(0, 1);
	texCoord[1] = float2(1, 1);
	texCoord[2] = float2(0, 0);
	texCoord[3] = float2(1, 0);

	PS_In outputVert;
	for (int i = 0; i < 4; i++)
	{
		outputVert.position = mul(Projection, mul(View, mul(World, float4(vert[i], 1.0f))));
		outputVert.world = float4(vert[i], 1.0f);
		outputVert.texCoord = texCoord[i];
		outputVert.normal = float4(planeNormal, 1.0f);

		output.Append(outputVert);
	}
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_In input) : SV_TARGET
{
	return float4(textureDiffuse.Sample(samLinear, input.texCoord).rgba);
}