#include <Graphics\Window.h>
#include <Graphics\D3D11Graphics.h>
#include "wtypes.h"

#include <WinUser.h>

//--------------------------------------------------------------------------------------
// Create and initialise window
//--------------------------------------------------------------------------------------
HRESULT Window::InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	// Register class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_TUTORIAL1);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = "Stratus Engine";
	wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_TUTORIAL1);
	if (!RegisterClassEx(&wcex))
		return E_FAIL;

	// Create window
	g_hInst = hInstance;

	RECT rc;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rc, 0);

	g_hWnd = CreateWindow("Stratus Engine", "Stratus Engine",
		WS_POPUP,
		CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
		NULL);
	if (!g_hWnd)
		return E_FAIL;

	SetWindowPos(g_hWnd, 0, 0, 0, rc.right, rc.bottom, 0);

	ShowWindow(g_hWnd, SW_MAXIMIZE);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Windows procedure to handle window messages
//--------------------------------------------------------------------------------------
LRESULT CALLBACK Window::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}