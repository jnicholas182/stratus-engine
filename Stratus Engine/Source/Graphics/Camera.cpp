#include <Graphics\Camera.h>
#include <Graphics\D3D11Graphics.h>
#include <Input\Input.h>

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Initialise camera including position data, spline data and buffers
//--------------------------------------------------------------------------------------
HRESULT Camera::Initialise(XMFLOAT3 position, XMFLOAT3 direction)
{
	cameraData = new CameraData();
	cameraData->Position = position;
	cameraData->Direction = direction;
	cameraData->Orientation = XMFLOAT3(XM_PI, -XM_PIDIV4, 0.0f);
	cameraData->Up = XMFLOAT3(0.0f, 1.0f, 0.0f);
	HRESULT hr = BufferFactory::CreateConstantBuffer<ViewMatrix>(&ViewBuffer);
	if (FAILED(hr))
		return hr;
	D3D11Graphics::getInstance().getContext()->GSSetConstantBuffers(1, 1, &ViewBuffer);
	D3D11Graphics::getInstance().getContext()->VSSetConstantBuffers(1, 1, &ViewBuffer);
	D3D11Graphics::getInstance().getContext()->DSSetConstantBuffers(1, 1, &ViewBuffer);

	hr = BufferFactory::CreateConstantBuffer<CameraConstBuffer>(&CameraDataBuffer);
	if (FAILED(hr))
		return hr;

	D3D11Graphics::getInstance().getContext()->GSSetConstantBuffers(3, 1, &CameraDataBuffer);
	D3D11Graphics::getInstance().getContext()->DSSetConstantBuffers(3, 1, &CameraDataBuffer);
	D3D11Graphics::getInstance().getContext()->PSSetConstantBuffers(3, 1, &CameraDataBuffer);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Update camera based on user input
//--------------------------------------------------------------------------------------
void Camera::Update(float previousFrameDuration)
{
	ApplyRotation(*cameraData);

	ViewMatrix viewMatrix;
	viewMatrix.View = XMMatrixLookAtLH(XMLoadFloat3(&cameraData->Position), XMLoadFloat3(&cameraData->Direction), XMLoadFloat3(&cameraData->Up));
	D3D11Graphics::getInstance().getContext()->UpdateSubresource(ViewBuffer, 0, NULL, &viewMatrix, 0, 0);

	CameraConstBuffer *camera = new CameraConstBuffer(); camera->Position = cameraData->Position;
	D3D11Graphics::getInstance().getContext()->UpdateSubresource(CameraDataBuffer, 0, NULL, camera, 0, 0);
}

//--------------------------------------------------------------------------------------
// Apply correct rotation to camera based on first person camera principles
//--------------------------------------------------------------------------------------
void Camera::ApplyRotation(CameraData& cameraData)
{
	if (cameraData.Orientation.x < 0.1f)
		cameraData.Orientation.x = 0.1f;
	if (cameraData.Orientation.x > XM_PI - 0.1f)
		cameraData.Orientation.x = XM_PI - 0.1f;
		
	// calculate the look at rotation y
	float tempZLoc = sin(cameraData.Orientation.x);
	float tempYLoc = cos(cameraData.Orientation.x);
	float tempXLoc = 0;

	// calculate the look at rotation x
	float newX = tempZLoc * cos(cameraData.Orientation.y);
	float newZ = tempZLoc * sin(cameraData.Orientation.y);
	tempXLoc = newX;
	tempZLoc = newZ;

	cameraData.Direction.x = cameraData.Position.x + tempXLoc;
	cameraData.Direction.y = cameraData.Position.y + tempYLoc;
	cameraData.Direction.z = cameraData.Position.z + tempZLoc;
}

//--------------------------------------------------------------------------------------
// Return camera data (position, direction, up, orientation)
//--------------------------------------------------------------------------------------
CameraData* Camera::GetCameraData()
{
	return cameraData;
}

//--------------------------------------------------------------------------------------
// Clean Up camera resources
//--------------------------------------------------------------------------------------
void Camera::CleanUp()
{
	ViewBuffer->Release();
	CameraDataBuffer->Release();
}