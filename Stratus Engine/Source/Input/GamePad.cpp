#include <Windows.h>
#include <Input\GamePad.h>
#pragma comment(lib, "Xinput.lib")

XButtonIDs XButtons;

//--------------------------------------------------------------------------------------
// Create a gamepad using controller index (eg player 1 / 2 / 3 / 4)
//--------------------------------------------------------------------------------------
Gamepad::Gamepad(int a_iIndex)
{
	m_iGamepadIndex = a_iIndex - 1;
	prevState = GetState();
	m_State = GetState();

	for (int i = 0; i < ButtonCount; i++)
	{
		bPrev_ButtonStates[i] = false;
		bButtonStates[i] = false;
		bGamepad_ButtonsDown[i] = false;
	}
}

//--------------------------------------------------------------------------------------
// Update current and previous state of controller input
//--------------------------------------------------------------------------------------
void Gamepad::Update()
{
	prevState = m_State;
	m_State = GetState();

	for (int i = 0; i < ButtonCount; i++)
	{
		bButtonStates[i] = (m_State.Gamepad.wButtons & XINPUT_Buttons[i]) == XINPUT_Buttons[i];

		bGamepad_ButtonsDown[i] = !bPrev_ButtonStates[i] && bButtonStates[i];
	}
}

//--------------------------------------------------------------------------------------
// Check if a button of a certain ID is currently being pressed
//--------------------------------------------------------------------------------------
bool Gamepad::GetButtonDown(int a_iButton)
{
	return bGamepad_ButtonsDown[a_iButton];
}

//--------------------------------------------------------------------------------------
// Return the current state of the controller
//--------------------------------------------------------------------------------------
XINPUT_STATE Gamepad::GetState()
{
	XINPUT_STATE GamepadState;
	ZeroMemory(&GamepadState, sizeof(XINPUT_STATE));
	XInputGetState(m_iGamepadIndex, &GamepadState);
	return GamepadState;
}

//--------------------------------------------------------------------------------------
// Return index of the gamepad
//--------------------------------------------------------------------------------------
int Gamepad::GetIndex()
{
	return m_iGamepadIndex;
}

//--------------------------------------------------------------------------------------
// Check if the controller is connected
//--------------------------------------------------------------------------------------
bool Gamepad::Connected()
{
	ZeroMemory(&m_State, sizeof(XINPUT_STATE));
	DWORD Result = XInputGetState(m_iGamepadIndex, &m_State);
	if (Result == ERROR_SUCCESS)
		return true;
	else
		return false;
}

//--------------------------------------------------------------------------------------
// Return value of left stick in the x-axis
//--------------------------------------------------------------------------------------
float Gamepad::LeftStick_X()
{
	short sX = m_State.Gamepad.sThumbLX;
	return (static_cast<float>(sX) / 32768.0f);
}

//--------------------------------------------------------------------------------------
// Return value of left stick in the y-axis
//--------------------------------------------------------------------------------------
float Gamepad::LeftStick_Y()
{
	short sY = m_State.Gamepad.sThumbLY;
	return (static_cast<float>(sY) / 32768.0f);
}

//--------------------------------------------------------------------------------------
// Return value of right stick in the x-axis
//--------------------------------------------------------------------------------------
float Gamepad::RightStick_X()
{
	short sX = m_State.Gamepad.sThumbRX;
	return (static_cast<float>(sX) / 32768.0f);
}

//--------------------------------------------------------------------------------------
// Return value of right stick in the y-axis
//--------------------------------------------------------------------------------------
float Gamepad::RightStick_Y()
{
	short sY = m_State.Gamepad.sThumbRY;
	return (static_cast<float>(sY) / 32768.0f);
}

//--------------------------------------------------------------------------------------
// Return value of left trigger
//--------------------------------------------------------------------------------------
float Gamepad::LeftTrigger()
{
	BYTE Trigger = m_State.Gamepad.bLeftTrigger;
	if (Trigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
		return Trigger / 255.0f;

	return 0.0f;
}

//--------------------------------------------------------------------------------------
// Return value of right trigger
//--------------------------------------------------------------------------------------
float Gamepad::RightTrigger()
{
	BYTE Trigger = m_State.Gamepad.bRightTrigger;

	if (Trigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
		return Trigger / 255.0f;

	return 0.0f;
}

//--------------------------------------------------------------------------------------
// Check if the left stick is within the dead-zone
//--------------------------------------------------------------------------------------
bool Gamepad::LStick_InDeadzone()
{
	short sX = m_State.Gamepad.sThumbLX;
	short sY = m_State.Gamepad.sThumbLY;

	if (sX > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE ||
		sX < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
		return false;

	if (sY > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE ||
		sY < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
		return false;

	return true;
}

//--------------------------------------------------------------------------------------
// Check if the right stick is within the dead-zone
//--------------------------------------------------------------------------------------
bool Gamepad::RStick_InDeadzone()
{
	short sX = m_State.Gamepad.sThumbRX;
	short sY = m_State.Gamepad.sThumbRY;

	if (sX > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE ||
		sX < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
		return false;

	if (sY > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE ||
		sY < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
		return false;

	return true;
}

//--------------------------------------------------------------------------------------
// Check if button with a certain ID is pressed
//--------------------------------------------------------------------------------------
bool Gamepad::GetButtonPressed(int a_iButton)
{
	if (m_State.Gamepad.wButtons & XINPUT_Buttons[a_iButton])
		return true;
	return false;
}

//--------------------------------------------------------------------------------------
// Check if button with a certain ID is released
//--------------------------------------------------------------------------------------
bool Gamepad::GetButtonReleased(int a_iButton)
{
	if (prevState.Gamepad.wButtons & XINPUT_Buttons[a_iButton])
		if (!(m_State.Gamepad.wButtons & XINPUT_Buttons[a_iButton]))
			return true;
	return false;
}

//--------------------------------------------------------------------------------------
// Initialise indexes of each button on the Xbox 360 Gamepad
//--------------------------------------------------------------------------------------
XButtonIDs::XButtonIDs()
{
	A = 0;
	B = 1;
	X = 2;
	Y = 3;

	DPad_Up = 4;
	DPad_Down = 5;
	DPad_Left = 6;
	DPad_Right = 7;

	L_Shoulder = 8;
	R_Shoulder = 9;

	L_Thumbstick = 10;
	R_Thumbstick = 11;

	Start = 12;
	Back = 13;
}