#include <Input\Input.h>
#include <Input\GamePad.h>
#include <Graphics\D3D11Graphics.h>
#include <Game\GameObjects\BaseObjects\GameObject.h>
#include <Game\GameObjects\BaseObjects\SkyboxObject.h>
#include <Graphics\Camera.h>

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Process keyboard input and react accordingly
//--------------------------------------------------------------------------------------
void Input::ReceiveKeyboardInput(float previousFrameDuration, HWND g_hWnd, std::vector<GameObject*> &gameObjects, SkyboxObject* skybox)
{
	CameraData* camData = Camera::getInstance().GetCameraData();

	float movement = previousFrameDuration * CAM_MOVE_SPEED;
	float rotation = 2.0f * previousFrameDuration;

	if (KEY_TOGGLED(VK_ESCAPE))
	{
		PostMessage(g_hWnd, WM_DESTROY, 0, 0);
	}
	if (KEY_DOWN(VK_UP))
	{
		camData->Orientation.x -= rotation;
	}
	if (KEY_DOWN(VK_DOWN))
	{
		camData->Orientation.x += rotation;
	}
	if (KEY_DOWN(VK_LEFT))
	{
		camData->Orientation.y += rotation;
	}
	if (KEY_DOWN(VK_RIGHT))
	{
		camData->Orientation.y -= rotation;
	}
	if (KEY_DOWN('W'))
	{
		camData->Position.z += movement * sin(camData->Orientation.y);
		camData->Position.x += movement * cos(camData->Orientation.y);
	}
	if (KEY_DOWN('S'))
	{
		camData->Position.z -= movement * sin(camData->Orientation.y);
		camData->Position.x -= movement * cos(camData->Orientation.y);
	}
	if (KEY_DOWN('A'))
	{
		camData->Position.z += movement * sin(camData->Orientation.y + XM_PIDIV2);
		camData->Position.x += movement * cos(camData->Orientation.y + XM_PIDIV2);
	}
	if (KEY_DOWN('D'))
	{
		camData->Position.z -= movement * sin(camData->Orientation.y + XM_PIDIV2);
		camData->Position.x -= movement * cos(camData->Orientation.y + XM_PIDIV2);
	}
	if (KEY_DOWN('E'))
	{
		camData->Position.y += movement;
	}
	if (KEY_DOWN('Q'))
	{
		camData->Position.y -= movement;
	}
	if (KEY_TOGGLED('1'))
	{
		for each (GameObject* gameObject in gameObjects)
		{
			gameObject->ToggleWireframe();
		}
		skybox->ToggleWireframe();
	}
	if (KEY_TOGGLED('2'))
	{
		for each (GameObject* gameObject in gameObjects)
		{
			gameObject->ToggleAntialiasing();
		}
		skybox->ToggleAntialiasing();
	}
}

//--------------------------------------------------------------------------------------
// Process gamepad input and react accordingly
//--------------------------------------------------------------------------------------
void Input::ReceiveControllerInput(float previousFrameDuration, HWND g_hWnd, Gamepad* gamepad, std::vector<GameObject*> &gameObjects, SkyboxObject* skybox)
{
	CameraData* camData = Camera::getInstance().GetCameraData();

	float movement = previousFrameDuration * CAM_MOVE_SPEED;
	float rotation = 2.0f * previousFrameDuration;
	if (!gamepad->RStick_InDeadzone())
	{
		camData->Orientation.y -= (rotation*gamepad->RightStick_X());
		camData->Orientation.x -= (rotation*gamepad->RightStick_Y());
	}
	if (!gamepad->LStick_InDeadzone())
	{
		camData->Position.z += ((movement*gamepad->LeftStick_Y()) * sin(camData->Orientation.y));
		camData->Position.x += ((movement*gamepad->LeftStick_Y()) * cos(camData->Orientation.y));
																						  
		camData->Position.z -= ((movement*gamepad->LeftStick_X()) * sin(camData->Orientation.y + XM_PIDIV2));
		camData->Position.x -= ((movement*gamepad->LeftStick_X()) * cos(camData->Orientation.y + XM_PIDIV2));
	}

	if (gamepad->GetButtonPressed(XButtons.L_Shoulder))
		camData->Position.y -= movement;

	if (gamepad->GetButtonPressed(XButtons.R_Shoulder))
		camData->Position.y += movement;

	if (gamepad->GetButtonReleased(XButtons.B))
	{
		for each (GameObject* gameObject in gameObjects)
		{
			gameObject->ToggleWireframe();
		}
		skybox->ToggleWireframe();
	}

	if (gamepad->GetButtonReleased(XButtons.Y))
	{
		for each (GameObject* gameObject in gameObjects)
		{
			gameObject->ToggleAntialiasing();
		}
		skybox->ToggleAntialiasing();
	}

	if (gamepad->GetButtonPressed(XButtons.Back))
	{
		PostMessage(g_hWnd, WM_DESTROY, 0, 0);
	}
}