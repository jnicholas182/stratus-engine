#include <Windows.h>
#include <Utils\resource.h>
#include <Graphics\D3D11Graphics.h>
#include <Game\GameObjects\BaseObjects\GameObject.h>
#include <Game\Game.h>
#include <Graphics\Window.h>

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Entry point to the program
//--------------------------------------------------------------------------------------
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	// Create Window and Graphics devices
	Window* window = new Window();

	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// Initialize Window and Graphics devices
	if (FAILED(window->InitWindow(hInstance, nCmdShow)))
		return 0;

	if (FAILED(D3D11Graphics::getInstance().InitDevice(window->g_hWnd)))
	{
		D3D11Graphics::getInstance().CleanUp();
		return 0;
	}

	// Create and Initialize a Game instance
	Game* game = new Game();
	game->Initialise();

	// Run game loop
	MSG msg = game->Run(window->g_hWnd);

	// Clean up Game and Graphics resources
	game->CleanUp();
	D3D11Graphics::getInstance().CleanUp();

	return (int)msg.wParam;
}