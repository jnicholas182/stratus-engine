#include <Game\GameObjects\WeatherObjects\RainEmitter.h>
#include <Graphics\D3D11Graphics.h>
#include <Utils\FileLoading\WICTextureLoader.h>
#include <Utils\ShaderFactory.h>
#include <Utils\Buffers\ComputeBuffers.h>
#include <Game\GameObjects\WeatherObjects\ParticleCloudObject.h>

#include <fstream>
#include <ctime>
#include <chrono>

using namespace DirectX;
using namespace std::chrono;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
RainEmitter::RainEmitter() : GameObject(){}

//--------------------------------------------------------------------------------------
// Initialise Object
//--------------------------------------------------------------------------------------
HRESULT RainEmitter::Initialise(Location updateLocation, std::string name, ParticleCloudObject* cloud, FallType fallType, XMFLOAT3 translation, XMFLOAT3 rotation, float scale)
{
	this->updateLocation = updateLocation;
	switch (fallType)
	{
	case Light:
		particleLimit = 100;
		batchSize = XMINT4(1, 1, 1, 0);
		break;
	case Heavy:
		particleLimit = 1000;
		batchSize = XMINT4(5, 2, 1, 0);
		break;
	case Torrential:
		particleLimit = 100000;
		batchSize = XMINT4(10, 10, 10, 0);
		break;
	}
	parentCloud = cloud;
	CreateParticles();

	D3D11_INPUT_ELEMENT_DESC newLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	arraySize = ARRAYSIZE(newLayout);
	layout = newLayout;

	if (FAILED(BufferFactory::CreateVertexBuffer<RainParticle>(vertices, &vertexBuffer)))
		return E_FAIL;

	if (FAILED(GameObject::Initialise(name, translation, rotation, scale, "RainParticleShader.fx")))
		return E_FAIL;

	if (FAILED(ShaderFactory::CreateShader<ID3D11GeometryShader>(&geometryShader, L"Shaders/RainParticleShader.fx", "GS", "gs_4_0")))
		return E_FAIL;

	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	D3D11Graphics::getInstance().getDevice()->CreateRasterizerState(&rasterizerDesc, &rasterizer);

	if (FAILED(CreateWICTextureFromFile(D3D11Graphics::getInstance().getDevice(), D3D11Graphics::getInstance().getContext(), L"Models/Particles/Rain.png", nullptr, &texture)))
		return E_FAIL;

	if (updateLocation == Location::CPU_MULTITHREADED)
		threadPool = new ThreadPool();
	else if (updateLocation == Location::GPU)
	{
		std::wstring computeShaderFile = L"Shaders/RainComputeShader.fx";

		if (FAILED(ShaderFactory::CreateShader<ID3D11ComputeShader>(&computeShader, computeShaderFile, "main", "cs_5_0")))
			return E_FAIL;

		computeInputBuffer = new ComputeInputBuffer();
		computeInputBuffer->Initialise<RainParticle>(vertices.size(), sizeof(RainParticle), vertices.data());

		computeOutputBuffer = new ComputeOutputBuffer();
		if (FAILED(computeOutputBuffer->Initialise(vertices.size(), sizeof(RainParticle))))
			return E_FAIL;

		if (FAILED(BufferFactory::CreateConstantBuffer<XMINT4>(&batchSizeBuffer)))
			return E_FAIL;
	}

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create Vertices for individual Particles
//--------------------------------------------------------------------------------------
void RainEmitter::CreateParticles()
{
	srand(GetTickCount());
	for (int i = 0; i < particleLimit; i++)
		vertices.push_back(RainParticle());
}

//--------------------------------------------------------------------------------------
// Update Object
//--------------------------------------------------------------------------------------
void RainEmitter::Update(float previousFrameDuration)
{
	stopWatch->Start();

	if (updateLocation == Location::CPU)
		UpdateOnCPU(previousFrameDuration);
	else if (updateLocation == Location::CPU_MULTITHREADED)
		UpdateOnCPUMultithreaded(previousFrameDuration);
	else if (updateLocation == Location::GPU)
		UpdateOnGPU(previousFrameDuration);

	D3D11Graphics::getInstance().getContext()->UpdateSubresource(this->vertexBuffer, 0, NULL, vertices.data(), 0, 0);

	stopWatch->Stop();
}

//--------------------------------------------------------------------------------------
// Update Object on CPU
//--------------------------------------------------------------------------------------
void RainEmitter::UpdateOnCPU(float previousFrameDuration)
{
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		if (vertices.at(i).Position.y < 0.0f)
			vertices.at(i).Reset(parentCloud->minBound, parentCloud->maxBound);
		else
			vertices.at(i).Update(previousFrameDuration);
	}
}

//--------------------------------------------------------------------------------------
// Update Object on CPU using multithreading
//--------------------------------------------------------------------------------------
void RainEmitter::UpdateOnCPUMultithreaded(float previousFrameDuration)
{
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		if (vertices.at(i).Position.y < 0.0f)
			vertices.at(i).Reset(parentCloud->minBound, parentCloud->maxBound);
		else
			threadPool->enqueue(boost::bind(&RainParticle::Update, &vertices.at(i), previousFrameDuration));
	}
}

//--------------------------------------------------------------------------------------
// Update Object on GPU
//--------------------------------------------------------------------------------------
void RainEmitter::UpdateOnGPU(float previousFrameDuration)
{
	D3D11Graphics::getInstance().getContext()->UpdateSubresource(computeInputBuffer->m_srcDataGPUBuffer, 0, NULL, vertices.data(), 0, 0);
	computeInputBuffer->SetActive(8);
	computeOutputBuffer->SetActive();

	D3D11Graphics::getInstance().getContext()->Dispatch(batchSize.x, batchSize.y, batchSize.z);
	ComputeBuffers::ClearBuffers();
	computeOutputBuffer->getGPUDestBufferCopy<RainParticle>(vertices.data());

	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		if (vertices.at(i).Position.y < 0.0f)
			vertices.at(i).Reset(parentCloud->minBound, parentCloud->maxBound);
	}
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void RainEmitter::SetShadersAndBuffers()
{
	GameObject::SetShadersAndBuffers();

	UINT stride = sizeof(RainParticle); UINT offset = 0;
	D3D11Graphics::getInstance().getContext()->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);

	D3D11Graphics::getInstance().getContext()->GSSetShader(geometryShader, NULL, 0);
	D3D11Graphics::getInstance().getContext()->PSSetShaderResources(0, 1, &texture);

	if (updateLocation == Location::GPU)
	{
		D3D11Graphics::getInstance().getContext()->CSSetShader(computeShader, NULL, 0);
		D3D11Graphics::getInstance().getContext()->UpdateSubresource(batchSizeBuffer, 0, NULL, &batchSize, 0, 0);
		D3D11Graphics::getInstance().getContext()->CSSetConstantBuffers(5, 1, &batchSizeBuffer);
	}
}

//--------------------------------------------------------------------------------------
// Set topology and sampler for object and draw
//--------------------------------------------------------------------------------------
void RainEmitter::Render()
{
	D3D11Graphics::getInstance().getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	D3D11Graphics::getInstance().SetSamplerLinear();

	D3D11Graphics::getInstance().getContext()->Draw(vertices.size(), 0);
}

//--------------------------------------------------------------------------------------
// Write profile data to file
//--------------------------------------------------------------------------------------
void RainEmitter::WriteDataToFile(std::string fileName)
{
	std::ofstream profileDataFile;
	profileDataFile.open(fileName, std::ios_base::app);

	profileDataFile << "==================================================\n";
	profileDataFile << "Object: " << name << "\n";
	profileDataFile << "Type: " << typeid(this).name() << "\n";
	std::string updateLocString;
	switch (updateLocation)
	{
	case CPU: updateLocString = "CPU";
		break;
	case CPU_MULTITHREADED: updateLocString = "CPU_MULTITHREADED";
		break;
	case GPU: updateLocString = "GPU";
		break;
	}
	profileDataFile << "Using: " << updateLocString << "\n";
	profileDataFile << "Iterations: " << stopWatch->getIterations() << "\n";
	profileDataFile << "Average Update Time: " << stopWatch->getAverage() << " milliseconds\n";

	profileDataFile.close();
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void RainEmitter::CleanUp()
{
	GameObject::CleanUp();
	geometryShader->Release();
	texture->Release();

	if (updateLocation == Location::CPU_MULTITHREADED)
		delete threadPool;
	else if (updateLocation == Location::GPU)
	{
		computeShader->Release();
		computeInputBuffer->CleanUp();
		computeOutputBuffer->CleanUp();
		batchSizeBuffer->Release();
	}
}