#include <Game\GameObjects\WeatherObjects\FogEmitter.h>
#include <Graphics\D3D11Graphics.h>
#include <Utils\FileLoading\WICTextureLoader.h>
#include <Utils\ShaderFactory.h>
#include <Utils\Buffers\ComputeBuffers.h>
#include <Graphics\Camera.h>

#include <algorithm>
#include <fstream>
#include <ctime>
#include <chrono>

using namespace DirectX;
using namespace std::chrono;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
FogEmitter::FogEmitter() : GameObject(){}

//--------------------------------------------------------------------------------------
// Initialise object
//--------------------------------------------------------------------------------------
HRESULT FogEmitter::Initialise(Location updateLocation, std::string name, Density density, DirectX::XMFLOAT3 minBound, DirectX::XMFLOAT3 maxBound, XMFLOAT3 translation, XMFLOAT3 rotation, float scale)
{
	this->updateLocation = updateLocation;
	switch (density)
	{
	case Low:
		particleLimit = 100;
		batchSize = XMINT4(1, 1, 1, 0);
		break;
	case Average:
		particleLimit = 1000;
		batchSize = XMINT4(5, 2, 1, 0);
		break;
	case High:
		particleLimit = 100000;
		batchSize = XMINT4(10, 10, 10, 0);
		break;
	}

	this->minBound = minBound; this->maxBound = maxBound;
	CreateParticles();

	D3D11_INPUT_ELEMENT_DESC newLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	arraySize = ARRAYSIZE(newLayout);
	layout = newLayout;

	if (FAILED(BufferFactory::CreateVertexBuffer<FogParticle>(vertices, &vertexBuffer)))
		return E_FAIL;

	if (FAILED(GameObject::Initialise(name, translation, rotation, scale, "FogParticleShader.fx")))
		return E_FAIL;

	if (FAILED(ShaderFactory::CreateShader<ID3D11GeometryShader>(&geometryShader, L"Shaders/FogParticleShader.fx", "GS", "gs_4_0")))
		return E_FAIL;

	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	D3D11Graphics::getInstance().getDevice()->CreateRasterizerState(&rasterizerDesc, &rasterizer);

	if (FAILED(CreateWICTextureFromFile(D3D11Graphics::getInstance().getDevice(), D3D11Graphics::getInstance().getContext(), L"Models/Particles/Fog.png", nullptr, &texture)))
		return E_FAIL;

	if (updateLocation == Location::CPU_MULTITHREADED)
		threadPool = new ThreadPool();
	else if (updateLocation == Location::GPU)
	{
		std::wstring computeShaderFile = L"Shaders/FogComputeShader.fx";

		if (FAILED(ShaderFactory::CreateShader<ID3D11ComputeShader>(&computeShader, computeShaderFile, "main", "cs_5_0")))
			return E_FAIL;

		computeInputBuffer = new ComputeInputBuffer();
		if (FAILED(computeInputBuffer->Initialise<FogParticle>(vertices.size(), sizeof(FogParticle), vertices.data())))
			return E_FAIL;

		computeOutputBuffer = new ComputeOutputBuffer();
		if (FAILED(computeOutputBuffer->Initialise(vertices.size(), sizeof(FogParticle))))
			return E_FAIL;

		if (FAILED(BufferFactory::CreateConstantBuffer<XMINT4>(&batchSizeBuffer)))
			return E_FAIL;
	}

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Vector comparison function to sort from back to front
//--------------------------------------------------------------------------------------
bool compareByDistance(const FogParticle &a, const FogParticle &b)
{
	return Magnitude(a.Position - Camera::getInstance().GetCameraData()->Position) > Magnitude(b.Position - Camera::getInstance().GetCameraData()->Position);
}

//--------------------------------------------------------------------------------------
// Create Vertices for individual Particles
//--------------------------------------------------------------------------------------
void FogEmitter::CreateParticles()
{
	srand(GetTickCount());
	for (int i = 0; i < particleLimit; i++)
		vertices.push_back(FogParticle(minBound, maxBound));
}

//--------------------------------------------------------------------------------------
// Update Object
//--------------------------------------------------------------------------------------
void FogEmitter::Update(float previousFrameDuration)
{
	stopWatch->Start();

	if (updateLocation == Location::CPU)
		UpdateOnCPU(previousFrameDuration);
	else if (updateLocation == Location::CPU_MULTITHREADED)
		UpdateOnCPUMultithreaded(previousFrameDuration);
	else if (updateLocation == Location::GPU)
		UpdateOnGPU(previousFrameDuration);

	std::sort(vertices.begin(), vertices.end(), &compareByDistance);
	D3D11Graphics::getInstance().getContext()->UpdateSubresource(this->vertexBuffer, 0, NULL, vertices.data(), 0, 0);

	stopWatch->Stop();
}

//--------------------------------------------------------------------------------------
// Update Object on CPU
//--------------------------------------------------------------------------------------
void FogEmitter::UpdateOnCPU(float previousFrameDuration)
{
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		if (vertices.at(i).Position.x > maxBound.x || vertices.at(i).Position.x < minBound.x ||
			vertices.at(i).Position.z > maxBound.z || vertices.at(i).Position.z < minBound.z)
			vertices.at(i).Reset(minBound, maxBound);
		else
			vertices.at(i).Update(previousFrameDuration, minBound, maxBound);
	}
}

//--------------------------------------------------------------------------------------
// Update Object on CPU using multithreading
//--------------------------------------------------------------------------------------
void FogEmitter::UpdateOnCPUMultithreaded(float previousFrameDuration)
{
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		if (vertices.at(i).Position.x > maxBound.x || vertices.at(i).Position.x < minBound.x ||
			vertices.at(i).Position.z > maxBound.z || vertices.at(i).Position.z < minBound.z)
			vertices.at(i).Reset(minBound, maxBound);
		else
			threadPool->enqueue(boost::bind(&FogParticle::Update, &vertices.at(i), previousFrameDuration, minBound, maxBound));
	}
}

//--------------------------------------------------------------------------------------
// Update Object on GPU
//--------------------------------------------------------------------------------------
void FogEmitter::UpdateOnGPU(float previousFrameDuration)
{
	D3D11Graphics::getInstance().getContext()->UpdateSubresource(computeInputBuffer->m_srcDataGPUBuffer, 0, NULL, vertices.data(), 0, 0);
	computeInputBuffer->SetActive(10);
	computeOutputBuffer->SetActive();

	D3D11Graphics::getInstance().getContext()->Dispatch(batchSize.x, batchSize.y, batchSize.z);
	ComputeBuffers::ClearBuffers();
	computeOutputBuffer->getGPUDestBufferCopy<FogParticle>(vertices.data());

	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		if (vertices.at(i).Position.x > maxBound.x || vertices.at(i).Position.x < minBound.x ||
			vertices.at(i).Position.z > maxBound.z || vertices.at(i).Position.z < minBound.z)
			vertices.at(i).Reset(minBound, maxBound);
	}
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void FogEmitter::SetShadersAndBuffers()
{
	GameObject::SetShadersAndBuffers();

	UINT stride = sizeof(FogParticle); UINT offset = 0;
	D3D11Graphics::getInstance().getContext()->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);

	D3D11Graphics::getInstance().getContext()->GSSetShader(geometryShader, NULL, 0);
	D3D11Graphics::getInstance().getContext()->PSSetShaderResources(0, 1, &texture);

	if (updateLocation == Location::GPU)
	{
		D3D11Graphics::getInstance().getContext()->CSSetShader(computeShader, NULL, 0);
		D3D11Graphics::getInstance().getContext()->UpdateSubresource(batchSizeBuffer, 0, NULL, &batchSize, 0, 0);
		D3D11Graphics::getInstance().getContext()->CSSetConstantBuffers(5, 1, &batchSizeBuffer);
	}
}

//--------------------------------------------------------------------------------------
// Set topology and sampler for object and draw
//--------------------------------------------------------------------------------------
void FogEmitter::Render()
{
	D3D11Graphics::getInstance().getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	D3D11Graphics::getInstance().SetSamplerLinear();

	D3D11Graphics::getInstance().getContext()->Draw(vertices.size(), 0);
}

//--------------------------------------------------------------------------------------
// Write profile data to file
//--------------------------------------------------------------------------------------
void FogEmitter::WriteDataToFile(std::string fileName)
{
	std::ofstream profileDataFile;
	profileDataFile.open(fileName, std::ios_base::app);

	profileDataFile << "==================================================\n";
	profileDataFile << "Object: " << name << "\n";
	profileDataFile << "Type: " << typeid(this).name() << "\n";
	std::string updateLocString;
	switch (updateLocation)
	{
	case CPU: updateLocString = "CPU";
		break;
	case CPU_MULTITHREADED: updateLocString = "CPU_MULTITHREADED";
		break;
	case GPU: updateLocString = "GPU";
		break;
	}
	profileDataFile << "Using: " << updateLocString << "\n";
	profileDataFile << "Iterations: " << stopWatch->getIterations() << "\n";
	profileDataFile << "Average Update Time: " << stopWatch->getAverage() << " milliseconds\n";

	profileDataFile.close();
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void FogEmitter::CleanUp()
{
	GameObject::CleanUp();
	geometryShader->Release();
	texture->Release();

	if (updateLocation == Location::CPU_MULTITHREADED)
		delete threadPool;
	else if (updateLocation == Location::GPU)
	{
		computeShader->Release();
		computeInputBuffer->CleanUp();
		computeOutputBuffer->CleanUp();
		batchSizeBuffer->Release();
	}
}