#include <Game\GameObjects\WeatherObjects\SimplexCloudObject.h> //must go first to avoid boost winsock re-include error

#include <fstream>
#include <boost\bind.hpp>

#include <Utils\NoiseFactory.h>
#include <Utils\ShaderFactory.h>
#include <Utils\Buffers\ComputeBuffers.h>

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
SimplexCloudObject::SimplexCloudObject() : GameObject(){}

//--------------------------------------------------------------------------------------
// Initialise object
//--------------------------------------------------------------------------------------
HRESULT SimplexCloudObject::Initialise(Location updateLocation, std::string name, CloudCover type, float altitude, XMFLOAT3 translation, XMFLOAT3 rotation, float scale)
{
	this->updateLocation = updateLocation;
	cloudData.yAnimationVal = 0.0f; cloudData.zAnimationVal = 0.0f;
	switch (type)
	{
	case Cloud_Clear:
		cloudData.cover = 100.0f; cloudData.sharpness = 0.95f;
		yAnimationSpeed = 0.1f; zAnimationSpeed = 0.1f;
		break;
	case Cloud_Slight:
		cloudData.cover = 140.0f; cloudData.sharpness = 0.95f;
		yAnimationSpeed = 0.1f; zAnimationSpeed = 0.1f;
		break;
	case Cloud_Overcast:
		cloudData.cover = 200.0f; cloudData.sharpness = 0.97f;
		yAnimationSpeed = 0.25f; zAnimationSpeed = 0.25f;
		break;
	case Cloud_Thick:
		cloudData.cover = 240.0f; cloudData.sharpness = 0.97f;
		yAnimationSpeed = 0.25f; zAnimationSpeed = 0.25f;
		break;
	}
	width = height = 256;

	int randNum = rand() % (50000 - 1 + 1) + 1;
	noiseGenerator = new SimplexNoise(randNum, cloudData.cover, cloudData.sharpness);

	CreateMesh(2000.0f, altitude);
	if (FAILED(CreateTexture()))
		return E_FAIL;

	D3D11_INPUT_ELEMENT_DESC newLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	arraySize = ARRAYSIZE(newLayout);
	layout = newLayout;

	if (FAILED(BufferFactory::CreateVertexBuffer<Vertex>(vertices, &vertexBuffer)))
		return E_FAIL;

	if (FAILED(BufferFactory::CreateIndexBuffer(indices, &indexBuffer)))
		return E_FAIL;

	if (FAILED(GameObject::Initialise(name, translation, rotation, scale, "SimplexCloudShader.fx")))
		return E_FAIL;

	textureData = new UINT32[width * height];

	if (updateLocation == Location::CPU_MULTITHREADED)
		threadPool = new ThreadPool();
	else if (updateLocation == Location::GPU)
	{
		std::wstring computeShaderFile = L"Shaders/SimplexComputeShader.fx";

		if (FAILED(ShaderFactory::CreateShader<ID3D11ComputeShader>(&computeShader, computeShaderFile, "main", "cs_5_0")))
			return E_FAIL;

		computeOutputBuffer = new ComputeOutputBuffer();
		computeOutputBuffer->Initialise(width * height, sizeof(int));

		if (FAILED(BufferFactory::CreateConstantBuffer<XMINT4>(&batchSizeBuffer)))
			return E_FAIL;
		batchSize = XMINT4(4, 4, 4, 0);

		if (FAILED(BufferFactory::CreateConstantBuffer<PermutationTableStruct>(&permutationTableBuffer)))
			return E_FAIL;
		PermutationTableStruct* tempTable = new PermutationTableStruct();
		std::copy(noiseGenerator->permutation.begin(), noiseGenerator->permutation.end(), tempTable->permutationTable);
		D3D11Graphics::getInstance().getContext()->UpdateSubresource(permutationTableBuffer, 0, NULL, tempTable, 0, 0);
		D3D11Graphics::getInstance().getContext()->CSSetConstantBuffers(6, 1, &permutationTableBuffer);

		if (FAILED(BufferFactory::CreateConstantBuffer<PerlinCloudData>(&cloudDataBuffer)))
			return E_FAIL;
		D3D11Graphics::getInstance().getContext()->CSSetConstantBuffers(7, 1, &cloudDataBuffer);
	}
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create quad to render texture to
//--------------------------------------------------------------------------------------
void SimplexCloudObject::CreateMesh(float size, float altitude)
{
	vertices.push_back(Vertex(XMFLOAT3((-size / 2.0f), altitude, (-size / 2.0f)), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f)));
	vertices.push_back(Vertex(XMFLOAT3((size / 2.0f), altitude, (-size / 2.0f)), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f)));
	vertices.push_back(Vertex(XMFLOAT3((-size / 2.0f), altitude, (size / 2.0f)), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f)));
	vertices.push_back(Vertex(XMFLOAT3((size / 2.0f), altitude, (size / 2.0f)), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f)));

	int ind[] = { 0, 1, 2, 2, 1, 3 };
	indices.insert(indices.end(), ind, ind+6);
}

//--------------------------------------------------------------------------------------
// Create a blank texture to be modified in update loop
//--------------------------------------------------------------------------------------
HRESULT SimplexCloudObject::CreateTexture()
{
	D3D11_SUBRESOURCE_DATA tbsd;
	ZeroMemory(&tbsd, sizeof(tbsd));

	int *buf = new int[width*height];
	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++)
			buf[i*width + j] = 0xffffffff;

	tbsd.pSysMem = (void *)buf;
	tbsd.SysMemPitch = width * 4;
	tbsd.SysMemSlicePitch = width*height * 4;
	
	D3D11_TEXTURE2D_DESC tdesc;
	ZeroMemory(&tdesc, sizeof(tdesc));

	tdesc.Width = width;
	tdesc.Height = height;
	tdesc.MipLevels = 1;
	tdesc.ArraySize = 1;

	tdesc.SampleDesc.Count = 1;
	tdesc.SampleDesc.Quality = 0;
	tdesc.Usage = D3D11_USAGE_DYNAMIC;
	tdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

	tdesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	tdesc.MiscFlags = 0;

	if (FAILED(D3D11Graphics::getInstance().getDevice()->CreateTexture2D(&tdesc, &tbsd, &tex)))
		return E_FAIL;

	D3D11_SHADER_RESOURCE_VIEW_DESC description;
	ZeroMemory(&description, sizeof(description));
	description.Format = tdesc.Format;
	description.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	description.Texture2D.MipLevels = tdesc.MipLevels;
	description.Texture2D.MostDetailedMip = tdesc.MipLevels - 1;

	if (FAILED(D3D11Graphics::getInstance().getDevice()->CreateShaderResourceView(tex, &description, &texture)))
		return E_FAIL;

	delete[] buf;
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void SimplexCloudObject::SetShadersAndBuffers()
{
	GameObject::SetShadersAndBuffers();
	UINT stride = sizeof(Vertex); UINT offset = 0;
	D3D11Graphics::getInstance().getContext()->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	D3D11Graphics::getInstance().getContext()->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	D3D11Graphics::getInstance().getContext()->GSSetShader(NULL, NULL, 0);
	D3D11Graphics::getInstance().getContext()->PSSetShaderResources(0, 1, &texture);

	if (updateLocation == Location::GPU)
	{
		D3D11Graphics::getInstance().getContext()->CSSetShader(computeShader, NULL, 0);
		D3D11Graphics::getInstance().getContext()->CSSetConstantBuffers(5, 1, &batchSizeBuffer);
		D3D11Graphics::getInstance().getContext()->UpdateSubresource(batchSizeBuffer, 0, NULL, &batchSize, 0, 0);
		D3D11Graphics::getInstance().getContext()->UpdateSubresource(cloudDataBuffer, 0, NULL, &cloudData, 0, 0);
		computeOutputBuffer->SetActive();
	}
}

//--------------------------------------------------------------------------------------
// Get bits between two points in a UINT32
//--------------------------------------------------------------------------------------
byte getBits(UINT32 data, unsigned lsb, unsigned msb)
{
	unsigned r = 0;
	for (unsigned i = lsb; i <= msb; i++)
		r |= 1 << i;
	return r & data;
}

//--------------------------------------------------------------------------------------
// Apply smoothing to texture by taking averages from adjacent pixels
//--------------------------------------------------------------------------------------
void SimplexCloudObject::smoothTexture()
{
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			float corners = 0.0f;
			float sides = 0.0f;
			if (y == 0)
			{
				if (x == 0)
				{
					corners = getBits(textureData[(y + 1)*width + (x + 1)], 0, 8) / 4.0f;
					sides = (getBits(textureData[y*width + (x + 1)], 0, 8) + getBits(textureData[(y + 1)*width + x], 0, 8)) / 4.0f;
				}
				else if (x == width - 1)
				{
					corners = getBits(textureData[(y + 1)*width + (x - 1)], 0, 8) / 4.0f;
					sides = (getBits(textureData[y*width + (x - 1)], 0, 8) + getBits(textureData[(y + 1)*width + x], 0, 8)) / 4.0f;
				}
				else
				{
					corners = (getBits(textureData[(y + 1)*width + (x - 1)], 0, 8) + getBits(textureData[(y + 1)*width + (x + 1)], 0, 8)) / 8.0f;
					sides = (getBits(textureData[y*width + (x - 1)], 0, 8) + getBits(textureData[y*width + (x + 1)], 0, 8) +
							 getBits(textureData[(y + 1)*width + x], 0, 8)) / 6.0f;
				}
			}
			else if (y == height - 1)
			{
				if (x == 0)
				{
					corners = getBits(textureData[(y - 1)*width + (x + 1)], 0, 8) / 4.0f;
					sides = (getBits(textureData[y*width + (x + 1)], 0, 8) + getBits(textureData[(y - 1)*width + x], 0, 8)) / 4.0f;
				}
				else if (x == width - 1)
				{
					corners = getBits(textureData[(y - 1)*width + (x - 1)], 0, 8) / 4.0f;
					sides = (getBits(textureData[y*width + (x - 1)], 0, 8) + getBits(textureData[(y - 1)*width + x], 0, 8)) / 4.0f;
				}
				else
				{
					corners = (getBits(textureData[(y - 1)*width + (x - 1)], 0, 8) + getBits(textureData[(y - 1)*width + (x + 1)], 0, 8)) / 8.0f;
					sides = (getBits(textureData[y*width + (x - 1)], 0, 8) + getBits(textureData[y*width + (x + 1)], 0, 8) +
							 getBits(textureData[(y - 1)*width + x], 0, 8)) / 6.0f;
				}
			}
			else
			{
				if (x == 0)
				{
					corners = (getBits(textureData[(y - 1)*width + (x + 1)], 0, 8) + getBits(textureData[(y + 1)*width + (x + 1)], 0, 8)) / 8.0f;
					sides = (getBits(textureData[(y - 1)*width + x], 0, 8) + getBits(textureData[(y + 1)*width + x], 0, 8) +
							 getBits(textureData[y*width + (x + 1)], 0, 8)) / 6.0f;
				}
				else if (x == width - 1)
				{
					corners = (getBits(textureData[(y - 1)*width + (x - 1)], 0, 8) + getBits(textureData[(y + 1)*width + (x - 1)], 0, 8)) / 8.0f;
					sides = (getBits(textureData[(y - 1)*width + x], 0, 8) + getBits(textureData[(y + 1)*width + x], 0, 8) +
							 getBits(textureData[y*width + (x - 1)], 0, 8)) / 6.0f;
				}
				else
				{
					corners = (getBits(textureData[(y - 1)*width + (x - 1)], 0, 8) + getBits(textureData[(y - 1)*width + (x + 1)], 0, 8) +
									 getBits(textureData[(y + 1)*width + (x - 1)], 0, 8) + getBits(textureData[(y + 1)*width + (x + 1)], 0, 8)) / 16.0f;

					sides = (getBits(textureData[y*width + (x - 1)], 0, 8) + getBits(textureData[y*width + (x + 1)], 0, 8) +
								   getBits(textureData[(y - 1)*width + x], 0, 8) + getBits(textureData[(y + 1)*width + x], 0, 8)) / 8.0f;
				}
			}

			float center = getBits(textureData[y*width + x], 0, 8) / 4.0f;
			byte value = byte(corners + sides + center);
			textureData[y*width + x] = (value << 24) | (value << 16) | (value << 8) | value;
		}
	}
}

//--------------------------------------------------------------------------------------
// Update Object
//--------------------------------------------------------------------------------------
void SimplexCloudObject::Update(float previousFrameDuration)
{
	stopWatch->Start();

	if(updateLocation == Location::CPU)
		UpdateOnCPU(previousFrameDuration);
	else if (updateLocation == Location::CPU_MULTITHREADED)
		UpdateOnCPUMultithreaded(previousFrameDuration);
	else if (updateLocation == Location::GPU)
		UpdateOnGPU(previousFrameDuration);

	smoothTexture();
	// Copy new texture for use in rendering
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	if (FAILED(D3D11Graphics::getInstance().getContext()->Map(tex, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource)))
		return;
	memcpy(mappedResource.pData, textureData, width * height * 4);
	D3D11Graphics::getInstance().getContext()->Unmap(tex, 0);

	cloudData.yAnimationVal += previousFrameDuration * yAnimationSpeed;
	cloudData.zAnimationVal += previousFrameDuration * zAnimationSpeed;

	stopWatch->Stop();
}

//--------------------------------------------------------------------------------------
// Update Object on CPU
//--------------------------------------------------------------------------------------
void SimplexCloudObject::UpdateOnCPU(float previousFrameDuration)
{
	float scale = 0.06f;
	for (int y = 0; y < height; y++)
	{
		double currentY = ((double)y * scale) + cloudData.yAnimationVal;
		for (int x = 0; x < width; x++)
		{
			byte value = byte(floor(255 * noiseGenerator->getNoiseValue(8, 0.25f, 1.0f, x*scale, currentY, cloudData.zAnimationVal)));
			textureData[y*width + x] = (value << 24) | (value << 16) | (value << 8) | value;
		}
	}
}

//--------------------------------------------------------------------------------------
// Update Object on CPU using multithreading
//--------------------------------------------------------------------------------------
void SimplexCloudObject::UpdateOnCPUMultithreaded(float previousFrameDuration)
{
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			threadPool->enqueue(boost::bind(&SimplexCloudObject::getPerlinValue, this, x, y));
		}
	}
}

//--------------------------------------------------------------------------------------
// Function to be threaded
//--------------------------------------------------------------------------------------
void SimplexCloudObject::getPerlinValue(int x, int y)
{
	float scale = 0.06f;
	double currentY = ((double)y * scale) + cloudData.yAnimationVal;
	byte value = byte(floor(255 * noiseGenerator->getNoiseValue(8, 0.25f, 1.0f, x*scale, currentY, cloudData.zAnimationVal)));
	textureData[y*width + x] = (value << 24) | (value << 16) | (value << 8) | value;
}

//--------------------------------------------------------------------------------------
// Update Object on GPU
//--------------------------------------------------------------------------------------
void SimplexCloudObject::UpdateOnGPU(float previousFrameDuration)
{
	D3D11Graphics::getInstance().getContext()->Dispatch(batchSize.x, batchSize.y, batchSize.z);
	ComputeBuffers::ClearBuffers();
	computeOutputBuffer->getGPUDestBufferCopy<UINT32>(textureData);
}

//--------------------------------------------------------------------------------------
// Set topology and sampler for object and draw
//--------------------------------------------------------------------------------------
void SimplexCloudObject::Render()
{
	D3D11Graphics::getInstance().getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	D3D11Graphics::getInstance().SetSamplerLinear();

	D3D11Graphics::getInstance().getContext()->DrawIndexed(indices.size(), 0, 0);
}

//--------------------------------------------------------------------------------------
// Write profile data to file
//--------------------------------------------------------------------------------------
void SimplexCloudObject::WriteDataToFile(std::string fileName)
{
	std::ofstream profileDataFile;
	profileDataFile.open(fileName, std::ios_base::app);

	profileDataFile << "==================================================\n";
	profileDataFile << "Object: " << name << "\n";
	profileDataFile << "Type: " << typeid(this).name() << "\n";
	std::string updateLocString;
	switch (updateLocation)
	{
	case CPU: updateLocString = "CPU";
		break;
	case CPU_MULTITHREADED: updateLocString = "CPU_MULTITHREADED";
		break;
	case GPU: updateLocString = "GPU";
		break;
	}
	profileDataFile << "Using: " << updateLocString << "\n";
	profileDataFile << "Iterations: " << stopWatch->getIterations() << "\n";
	profileDataFile << "Average Update Time: " << stopWatch->getAverage() << " milliseconds\n";

	profileDataFile.close();
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void SimplexCloudObject::CleanUp()
{
	GameObject::CleanUp();
	if (updateLocation == Location::CPU_MULTITHREADED)
		delete threadPool;
	else if (updateLocation == Location::GPU)
	{
		computeShader->Release();
		computeOutputBuffer->CleanUp();
		batchSizeBuffer->Release();
		permutationTableBuffer->Release();
		cloudDataBuffer->Release();
	}
}