#include <Game\GameObjects\WeatherObjects\SnowEmitter.h>
#include <Graphics\D3D11Graphics.h>
#include <Utils\FileLoading\WICTextureLoader.h>
#include <Utils\ShaderFactory.h>
#include <Utils\Buffers\ComputeBuffers.h>
#include <Game\GameObjects\WeatherObjects\ParticleCloudObject.h>

#include <fstream>
#include <ctime>
#include <chrono>

using namespace DirectX;
using namespace std::chrono;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
SnowEmitter::SnowEmitter() : GameObject(){}

//--------------------------------------------------------------------------------------
// Initialise object
//--------------------------------------------------------------------------------------
HRESULT SnowEmitter::Initialise(Location updateLocation, std::string name, ParticleCloudObject* cloud, FallType fallType, XMFLOAT3 translation, XMFLOAT3 rotation, float scale)
{
	this->updateLocation = updateLocation;
	switch (fallType)
	{
	case Light:
		particleLimit = 100;
		batchSize = XMINT4(1, 1, 1, 0);
		break;
	case Heavy:
		particleLimit = 1000;
		batchSize = XMINT4(5, 2, 1, 0);
		break;
	case Torrential:
		particleLimit = 100000;
		batchSize = XMINT4(10, 10, 10, 0);
		break;
	}
	parentCloud = cloud;
	CreateParticles();

	D3D11_INPUT_ELEMENT_DESC newLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "DIRECTION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXID", 0, DXGI_FORMAT_R32_SINT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	arraySize = ARRAYSIZE(newLayout);
	layout = newLayout;

	if (FAILED(BufferFactory::CreateVertexBuffer<SnowParticle>(vertices, &vertexBuffer)))
		return E_FAIL;

	if (FAILED(GameObject::Initialise(name, translation, rotation, scale, "SnowParticleShader.fx")))
		return E_FAIL;

	if (FAILED(ShaderFactory::CreateShader<ID3D11GeometryShader>(&geometryShader, L"Shaders/SnowParticleShader.fx", "GS", "gs_4_0")))
		return E_FAIL;

	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	D3D11Graphics::getInstance().getDevice()->CreateRasterizerState(&rasterizerDesc, &rasterizer);

	LoadTextures();

	if (updateLocation == Location::CPU_MULTITHREADED)
		threadPool = new ThreadPool();
	else if (updateLocation == Location::GPU)
	{
		std::wstring computeShaderFile = L"Shaders/SnowComputeShader.fx";

		if (FAILED(ShaderFactory::CreateShader<ID3D11ComputeShader>(&computeShader, computeShaderFile, "main", "cs_5_0")))
			return E_FAIL;

		computeInputBuffer = new ComputeInputBuffer();
		computeInputBuffer->Initialise<SnowParticle>(vertices.size(), sizeof(SnowParticle), vertices.data());

		computeOutputBuffer = new ComputeOutputBuffer();
		if (FAILED(computeOutputBuffer->Initialise(vertices.size(), sizeof(SnowParticle))))
			return E_FAIL;

		if (FAILED(BufferFactory::CreateConstantBuffer<XMINT4>(&batchSizeBuffer)))
			return E_FAIL;
	}

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create Vertices for individual Particles
//--------------------------------------------------------------------------------------
void SnowEmitter::CreateParticles()
{
	for (int i = 0; i < particleLimit; i++)
		vertices.push_back(SnowParticle());
}

//--------------------------------------------------------------------------------------
// Load all textures for snowflakes
//--------------------------------------------------------------------------------------
HRESULT SnowEmitter::LoadTextures()
{
	for (int i = 1; i < 7; i++)
	{
		ID3D11ShaderResourceView* loadedTex;
		std::string tex ("Models/Particles/Snowflake" + std::to_string(i) + ".png");
		if (FAILED(CreateWICTextureFromFile(D3D11Graphics::getInstance().getDevice(), D3D11Graphics::getInstance().getContext(), std::wstring(tex.begin(), tex.end()).c_str(), nullptr, &loadedTex)))
			return E_FAIL;
		textures.push_back(loadedTex);
	}
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Update Object
//--------------------------------------------------------------------------------------
void SnowEmitter::Update(float previousFrameDuration)
{
	stopWatch->Start();

	if (updateLocation == Location::CPU)
		UpdateOnCPU(previousFrameDuration);
	else if (updateLocation == Location::CPU_MULTITHREADED)
		UpdateOnCPUMultithreaded(previousFrameDuration);
	else if (updateLocation == Location::GPU)
		UpdateOnGPU(previousFrameDuration);

	D3D11Graphics::getInstance().getContext()->UpdateSubresource(this->vertexBuffer, 0, NULL, vertices.data(), 0, 0);

	stopWatch->Stop();
}

//--------------------------------------------------------------------------------------
// Update Object on CPU
//--------------------------------------------------------------------------------------
void SnowEmitter::UpdateOnCPU(float previousFrameDuration)
{
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		if (vertices.at(i).Position.y < 0.0f)
			vertices.at(i).Reset(parentCloud->minBound, parentCloud->maxBound);
		else
			vertices.at(i).Update(previousFrameDuration);
	}
}

//--------------------------------------------------------------------------------------
// Update Object on CPU using multithreading
//--------------------------------------------------------------------------------------
void SnowEmitter::UpdateOnCPUMultithreaded(float previousFrameDuration)
{
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		if (vertices.at(i).Position.y < 0.0f)
			vertices.at(i).Reset(parentCloud->minBound, parentCloud->maxBound);
		else
			threadPool->enqueue(boost::bind(&SnowParticle::Update, &vertices.at(i), previousFrameDuration));
	}
}

//--------------------------------------------------------------------------------------
// Update Object on GPU
//--------------------------------------------------------------------------------------
void SnowEmitter::UpdateOnGPU(float previousFrameDuration)
{
	D3D11Graphics::getInstance().getContext()->UpdateSubresource(computeInputBuffer->m_srcDataGPUBuffer, 0, NULL, vertices.data(), 0, 0);
	computeInputBuffer->SetActive(9);
	computeOutputBuffer->SetActive();

	D3D11Graphics::getInstance().getContext()->Dispatch(batchSize.x, batchSize.y, batchSize.z);
	ComputeBuffers::ClearBuffers();
	computeOutputBuffer->getGPUDestBufferCopy<SnowParticle>(vertices.data());

	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		if (vertices.at(i).Position.y < 0.0f)
			vertices.at(i).Reset(parentCloud->minBound, parentCloud->maxBound);
	}
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void SnowEmitter::SetShadersAndBuffers()
{
	GameObject::SetShadersAndBuffers();

	UINT stride = sizeof(SnowParticle); UINT offset = 0;
	D3D11Graphics::getInstance().getContext()->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);

	D3D11Graphics::getInstance().getContext()->GSSetShader(geometryShader, NULL, 0);

	for (unsigned int i = 0; i < textures.size(); i++)
	{
		D3D11Graphics::getInstance().getContext()->PSSetShaderResources(i + 2, 1, &textures[i]);
	}

	if (updateLocation == Location::GPU)
	{
		D3D11Graphics::getInstance().getContext()->CSSetShader(computeShader, NULL, 0);
		D3D11Graphics::getInstance().getContext()->UpdateSubresource(batchSizeBuffer, 0, NULL, &batchSize, 0, 0);
		D3D11Graphics::getInstance().getContext()->CSSetConstantBuffers(5, 1, &batchSizeBuffer);
	}
}

//--------------------------------------------------------------------------------------
// Set topology and sampler for object and draw
//--------------------------------------------------------------------------------------
void SnowEmitter::Render()
{
	D3D11Graphics::getInstance().getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	D3D11Graphics::getInstance().SetSamplerLinear();

	D3D11Graphics::getInstance().getContext()->Draw(vertices.size(), 0);
}

//--------------------------------------------------------------------------------------
// Write profile data to file
//--------------------------------------------------------------------------------------
void SnowEmitter::WriteDataToFile(std::string fileName)
{
	std::ofstream profileDataFile;
	profileDataFile.open(fileName, std::ios_base::app);

	profileDataFile << "==================================================\n";
	profileDataFile << "Object: " << name << "\n";
	profileDataFile << "Type: " << typeid(this).name() << "\n";
	std::string updateLocString;
	switch (updateLocation)
	{
	case CPU: updateLocString = "CPU";
		break;
	case CPU_MULTITHREADED: updateLocString = "CPU_MULTITHREADED";
		break;
	case GPU: updateLocString = "GPU";
		break;
	}
	profileDataFile << "Using: " << updateLocString << "\n";
	profileDataFile << "Iterations: " << stopWatch->getIterations() << "\n";
	profileDataFile << "Average Update Time: " << stopWatch->getAverage() << " milliseconds\n";

	profileDataFile.close();
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void SnowEmitter::CleanUp()
{
	GameObject::CleanUp();
	geometryShader->Release();
	for each (ID3D11ShaderResourceView* texture in textures)
	{
		texture->Release();
	}

	if (updateLocation == Location::CPU_MULTITHREADED)
		delete threadPool;
	else if (updateLocation == Location::GPU)
	{
		computeShader->Release();
		computeInputBuffer->CleanUp();
		computeOutputBuffer->CleanUp();
		batchSizeBuffer->Release();
	}
}