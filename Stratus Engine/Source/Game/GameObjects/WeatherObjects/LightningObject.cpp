#include <Game\GameObjects\WeatherObjects\LightningObject.h>
#include <Graphics\D3D11Graphics.h>
#include <Utils\FileLoading\WICTextureLoader.h>
#include <Utils\ShaderFactory.h>
#include <Utils\Buffers\ComputeBuffers.h>
#include <Game\GameObjects\WeatherObjects\ParticleCloudObject.h>

#include <fstream>
#include <ctime>
#include <chrono>

using namespace DirectX;
using namespace std::chrono;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
LightningObject::LightningObject() : GameObject(){}

//--------------------------------------------------------------------------------------
// Initialise object
//--------------------------------------------------------------------------------------
HRESULT LightningObject::Initialise(std::string name, ParticleCloudObject* parentCloud, XMFLOAT3 translation, XMFLOAT3 rotation, float scale)
{
	this->updateLocation = Location::CPU;
	this->parentCloud = parentCloud;

	D3D11_INPUT_ELEMENT_DESC newLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	arraySize = ARRAYSIZE(newLayout);
	layout = newLayout;

	if (FAILED(GameObject::Initialise(name, translation, rotation, scale, "LightningShader.fx")))
		return E_FAIL;

	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	D3D11Graphics::getInstance().getDevice()->CreateRasterizerState(&rasterizerDesc, &rasterizer);

	return S_OK;
}

XMFLOAT3 Perpendicular(XMFLOAT3 vector)
{
	XMFLOAT3 randomVector = XMFLOAT3(randomFloat(-1.0f, 1.0f), randomFloat(-1.0f, 1.0f), randomFloat(-1.0f, 1.0f));
	XMFLOAT3 PerpendicularVector = Cross(randomVector, vector);
	return PerpendicularVector;
}

//--------------------------------------------------------------------------------------
// Update Object
//--------------------------------------------------------------------------------------
void LightningObject::Update(float previousFrameDuration)
{
	vertices.clear();
	if (randomFloat(0.0f, 250.0f) < 3.0f)
	{
		stopWatch->Start();

		srand(GetTickCount());
		std::vector<LightningSegment> previousGenSegments;
		previousGenSegments.push_back(
			LightningSegment(
				XMFLOAT3(NormalDistribution::GetRandomValue(parentCloud->minBound.x, parentCloud->maxBound.x),
						 NormalDistribution::GetRandomValue(parentCloud->minBound.y, parentCloud->maxBound.y),
						 NormalDistribution::GetRandomValue(parentCloud->minBound.z, parentCloud->maxBound.z)),
				XMFLOAT3(NormalDistribution::GetRandomValue(parentCloud->minBound.x, parentCloud->maxBound.x),
						 0.0f,
						 NormalDistribution::GetRandomValue(parentCloud->minBound.z, parentCloud->maxBound.z))
			)
		);
		float offset = 7.0f;
		for (int generation = 1; generation < 6; generation++)
		{
			std::vector<LightningSegment> thisGenSegments;
			for each (LightningSegment segment in previousGenSegments)
			{
				XMFLOAT3 midPoint = (segment.StartPoint + segment.EndPoint) / 2.0f;
				//Offset midpoint by a random amount
				midPoint += Perpendicular(NormalizeXMFLOAT3(segment.EndPoint - segment.StartPoint))*randomFloat(-offset, offset);
				thisGenSegments.push_back(LightningSegment(segment.StartPoint, midPoint));
				thisGenSegments.push_back(LightningSegment(midPoint, segment.EndPoint));

				//50% chance to create a new segment off of the new split
				int spawn = rand() % 2;
				if (spawn)
				{
					float lengthScale = 0.7f;
					XMFLOAT3 direction = midPoint - segment.StartPoint;
					XMFLOAT3 splitEnd = direction*lengthScale + midPoint;
					thisGenSegments.push_back(LightningSegment(midPoint, splitEnd));
				}
			}
			offset /= 2.0f; // Shrink offset every generation to prevent massive offsets for small segments
			previousGenSegments = std::vector<LightningSegment>(thisGenSegments);
		}
		for each (LightningSegment segment in previousGenSegments)
		{
			vertices.push_back(segment.StartPoint);
			vertices.push_back(segment.EndPoint);
		}

		BufferFactory::CreateVertexBuffer<XMFLOAT3>(vertices, &vertexBuffer);
		D3D11Graphics::getInstance().getContext()->UpdateSubresource(this->vertexBuffer, 0, NULL, vertices.data(), 0, 0);

		stopWatch->Stop();
	}
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void LightningObject::SetShadersAndBuffers()
{
	GameObject::SetShadersAndBuffers();

	UINT stride = sizeof(XMFLOAT3); UINT offset = 0;
	D3D11Graphics::getInstance().getContext()->GSSetShader(NULL, NULL, 0);
	D3D11Graphics::getInstance().getContext()->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
}

//--------------------------------------------------------------------------------------
// Set topology and sampler for object and draw
//--------------------------------------------------------------------------------------
void LightningObject::Render()
{
	D3D11Graphics::getInstance().getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	D3D11Graphics::getInstance().SetSamplerLinear();

	D3D11Graphics::getInstance().getContext()->Draw(vertices.size(), 0);
}

//--------------------------------------------------------------------------------------
// Write profile data to file
//--------------------------------------------------------------------------------------
void LightningObject::WriteDataToFile(std::string fileName)
{
	std::ofstream profileDataFile;
	profileDataFile.open(fileName, std::ios_base::app);

	profileDataFile << "==================================================\n";
	profileDataFile << "Object: " << name << "\n";
	profileDataFile << "Type: " << typeid(this).name() << "\n";
	profileDataFile << "Using: CPU\n";
	profileDataFile << "Iterations: " << stopWatch->getIterations() << "\n";
	profileDataFile << "Average Update Time: " << stopWatch->getAverage() << " milliseconds\n";

	profileDataFile.close();
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void LightningObject::CleanUp()
{
	GameObject::CleanUp();
}