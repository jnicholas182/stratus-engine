#include <Game\GameObjects\WeatherObjects\ParticleCloudObject.h>
#include <Graphics\D3D11Graphics.h>
#include <Utils\FileLoading\WICTextureLoader.h>
#include <Utils\ShaderFactory.h>
#include <Utils\Buffers\ComputeBuffers.h>

#include <fstream>
#include <ctime>
#include <chrono>

using namespace DirectX;
using namespace std::chrono;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
ParticleCloudObject::ParticleCloudObject() : GameObject(){}

//--------------------------------------------------------------------------------------
// Initialise object
//--------------------------------------------------------------------------------------
HRESULT ParticleCloudObject::Initialise(Location updateLocation, std::string name, Size size, XMFLOAT3 position, XMFLOAT3 translation, XMFLOAT3 rotation, float scale)
{
	this->updateLocation = updateLocation;
	switch (size)
	{
	case Small:
		particleLimit = 100;
		batchSize = XMINT4(1, 1, 1, 0);
		xSize = 10.0f; ySize = 2.5f; zSize = 10.0f;
		break;
	case Medium:
		particleLimit = 1000;
		batchSize = XMINT4(5, 2, 1, 0);
		xSize = 15.0f; ySize = 3.75f; zSize = 15.0f;
		break;
	case Large:
		particleLimit = 10000;
		batchSize = XMINT4(5, 5, 4, 0);
		xSize = 20.0f; ySize = 5.0f; zSize = 20.0f;
		break;
	}
	this->position = position;
	minBound = XMFLOAT3(position.x - xSize, position.y - ySize, position.z - zSize);
	maxBound = XMFLOAT3(position.x + xSize, position.y + ySize, position.z + zSize);
	CreateParticles();

	D3D11_INPUT_ELEMENT_DESC newLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	arraySize = ARRAYSIZE(newLayout);
	layout = newLayout;

	if (FAILED(BufferFactory::CreateVertexBuffer<CloudParticle>(vertices, &vertexBuffer)))
		return E_FAIL;

	if (FAILED(GameObject::Initialise(name, translation, rotation, scale, "ParticleCloudShader.fx")))
		return E_FAIL;

	if (FAILED(ShaderFactory::CreateShader<ID3D11GeometryShader>(&geometryShader, L"Shaders/ParticleCloudShader.fx", "GS", "gs_4_0")))
		return E_FAIL;

	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	D3D11Graphics::getInstance().getDevice()->CreateRasterizerState(&rasterizerDesc, &rasterizer);

	if (FAILED(CreateWICTextureFromFile(D3D11Graphics::getInstance().getDevice(), D3D11Graphics::getInstance().getContext(), L"Models/Particles/Fog.png", nullptr, &texture)))
		return E_FAIL;

	if (updateLocation == Location::CPU_MULTITHREADED)
		threadPool = new ThreadPool();
	else if (updateLocation == Location::GPU)
	{
		std::wstring computeShaderFile = L"Shaders/ParticleCloudComputeShader.fx";

		if (FAILED(ShaderFactory::CreateShader<ID3D11ComputeShader>(&computeShader, computeShaderFile, "main", "cs_5_0")))
			return E_FAIL;

		computeInputBuffer = new ComputeInputBuffer();
		if (FAILED(computeInputBuffer->Initialise<CloudParticle>(vertices.size(), sizeof(CloudParticle), vertices.data())))
			return E_FAIL;

		computeOutputBuffer = new ComputeOutputBuffer();
		if (FAILED(computeOutputBuffer->Initialise(vertices.size(), sizeof(CloudParticle))))
			return E_FAIL;

		if (FAILED(BufferFactory::CreateConstantBuffer<XMINT4>(&batchSizeBuffer)))
			return E_FAIL;

		if (FAILED(BufferFactory::CreateConstantBuffer<XMFLOAT4>(&cloudCentreBuffer)))
			return E_FAIL;
		D3D11Graphics::getInstance().getContext()->CSSetConstantBuffers(9, 1, &cloudCentreBuffer);
	}
	return S_OK;
}

bool compareByDistance(const CloudParticle &a, const CloudParticle &b)
{
	return a.Position.y > b.Position.y;
}

//--------------------------------------------------------------------------------------
// Create Vertices for individual Particles
//--------------------------------------------------------------------------------------
void ParticleCloudObject::CreateParticles()
{
	srand(GetTickCount());
	for (int i = 0; i < particleLimit; i++)
	{
		bool pushedBack = false;
		while (!pushedBack)
		{
			bool matched = false;
			CloudParticle newParticle = CloudParticle(minBound, maxBound);
			for (unsigned int j = 0; j < vertices.size(); j++)
				if (vertices.at(j).Position.y == newParticle.Position.y)
					matched = true;
			if (!matched)
			{
				vertices.push_back(newParticle);
				pushedBack = true;
			}
		}
	}
}

//--------------------------------------------------------------------------------------
// Vector comparison functions
//--------------------------------------------------------------------------------------
bool compareX(CloudParticle i, CloudParticle j) { return i.Position.x < j.Position.x; }
bool compareY(CloudParticle i, CloudParticle j) { return i.Position.y < j.Position.y; }
bool compareZ(CloudParticle i, CloudParticle j) { return i.Position.z < j.Position.z; }

//--------------------------------------------------------------------------------------
// Update the x, y, z dimensions of the clouds based on particle positions
//--------------------------------------------------------------------------------------
void ParticleCloudObject::UpdateCloudSize()
{
	CloudParticle minX = *std::min_element(vertices.begin(), vertices.end(), compareX);
	CloudParticle minY = *std::min_element(vertices.begin(), vertices.end(), compareY);
	CloudParticle minZ = *std::min_element(vertices.begin(), vertices.end(), compareZ);
	minBound = XMFLOAT3(minX.Position.x, minY.Position.y, minZ.Position.z);

	CloudParticle maxX = *std::max_element(vertices.begin(), vertices.end(), compareX);
	CloudParticle maxY = *std::max_element(vertices.begin(), vertices.end(), compareY);
	CloudParticle maxZ = *std::max_element(vertices.begin(), vertices.end(), compareZ);
	maxBound = XMFLOAT3(maxX.Position.x, maxY.Position.y, maxZ.Position.z);
}

//--------------------------------------------------------------------------------------
// Update Object
//--------------------------------------------------------------------------------------
void ParticleCloudObject::Update(float previousFrameDuration)
{
	stopWatch->Start();

	//position.z -= previousFrameDuration * 0.5f;
	if (updateLocation == Location::CPU)
		UpdateOnCPU(previousFrameDuration);
	else if (updateLocation == Location::CPU_MULTITHREADED)
		UpdateOnCPUMultithreaded(previousFrameDuration);
	else if (updateLocation == Location::GPU)
		UpdateOnGPU(previousFrameDuration);

	std::sort(vertices.begin(), vertices.end(), &compareByDistance);
	D3D11Graphics::getInstance().getContext()->UpdateSubresource(this->vertexBuffer, 0, NULL, vertices.data(), 0, 0);

	UpdateCloudSize();

	stopWatch->Stop();
}

//--------------------------------------------------------------------------------------
// Update Object on CPU
//--------------------------------------------------------------------------------------
void ParticleCloudObject::UpdateOnCPU(float previousFrameDuration)
{
	float speed = previousFrameDuration * 0.001f;
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		XMFLOAT2 cohesion = NormalizeXMFLOAT2(XMFLOAT2(position.x - vertices.at(i).Position.x, position.z - vertices.at(i).Position.z)); //Aim for centre of cloud
		XMFLOAT2 separation = getSeparation(i); //Separate from other particles
		vertices.at(i).Velocity = vertices.at(i).Velocity + speed * NormalizeXMFLOAT2(XMFLOAT2(cohesion.x * 0.6f + (separation.x * 0.3f), cohesion.y * 0.6f + (separation.y * 0.3f)));
		vertices.at(i).Position.x += vertices.at(i).Velocity.x; vertices.at(i).Position.z += vertices.at(i).Velocity.y;
	}
}

//--------------------------------------------------------------------------------------
// Update Object on CPU using multithreading
//--------------------------------------------------------------------------------------
void ParticleCloudObject::UpdateOnCPUMultithreaded(float previousFrameDuration)
{
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		threadPool->enqueue(boost::bind(&ParticleCloudObject::ThreadedUpdate, this, previousFrameDuration, i));
	}
}

//--------------------------------------------------------------------------------------
// Threaded update function to be passed into threads
//--------------------------------------------------------------------------------------
void ParticleCloudObject::ThreadedUpdate(float previousFrameDuration, int index)
{
	float speed = previousFrameDuration * 0.001f;
	XMFLOAT2 cohesion = NormalizeXMFLOAT2(XMFLOAT2(position.x - vertices.at(index).Position.x, position.z - vertices.at(index).Position.z)); //Aim for centre of cloud
	XMFLOAT2 separation = getSeparation(index); //Separate from other particles
	vertices.at(index).Velocity = vertices.at(index).Velocity + speed * NormalizeXMFLOAT2(XMFLOAT2(cohesion.x * 0.6f + (separation.x * 0.3f), cohesion.y * 0.6f + (separation.y * 0.3f)));
	vertices.at(index).Position.x += vertices.at(index).Velocity.x; vertices.at(index).Position.z += vertices.at(index).Velocity.y;
}

//--------------------------------------------------------------------------------------
// Update Object on GPU
//--------------------------------------------------------------------------------------
void ParticleCloudObject::UpdateOnGPU(float previousFrameDuration)
{
	D3D11Graphics::getInstance().getContext()->UpdateSubresource(computeInputBuffer->m_srcDataGPUBuffer, 0, NULL, vertices.data(), 0, 0);
	computeInputBuffer->SetActive(11);
	computeOutputBuffer->SetActive();

	D3D11Graphics::getInstance().getContext()->Dispatch(batchSize.x, batchSize.y, batchSize.z);
	ComputeBuffers::ClearBuffers();
	computeOutputBuffer->getGPUDestBufferCopy<CloudParticle>(vertices.data());
}


//--------------------------------------------------------------------------------------
// Perform Separation part of flocking algorithm
//--------------------------------------------------------------------------------------
XMFLOAT2 ParticleCloudObject::getSeparation(int index)
{
	XMFLOAT2 averagePos = XMFLOAT2(0.0f, 0.0f);
	int neighbourCount = 0;
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		if (i != index)
		{
			averagePos.x += vertices.at(i).Position.x - vertices.at(index).Position.x;
			averagePos.y += vertices.at(i).Position.z - vertices.at(index).Position.z;
		}
	}
	averagePos.x /= vertices.size(); averagePos.y /= vertices.size();
	averagePos.x *= -1.0f; averagePos.y *= -1.0f;
	return NormalizeXMFLOAT2(averagePos);
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void ParticleCloudObject::SetShadersAndBuffers()
{
	GameObject::SetShadersAndBuffers();

	UINT stride = sizeof(CloudParticle); UINT offset = 0;
	D3D11Graphics::getInstance().getContext()->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);

	D3D11Graphics::getInstance().getContext()->GSSetShader(geometryShader, NULL, 0);
	D3D11Graphics::getInstance().getContext()->PSSetShaderResources(0, 1, &texture);

	if (updateLocation == Location::GPU)
	{
		D3D11Graphics::getInstance().getContext()->CSSetShader(computeShader, NULL, 0);
		D3D11Graphics::getInstance().getContext()->UpdateSubresource(batchSizeBuffer, 0, NULL, &batchSize, 0, 0);
		D3D11Graphics::getInstance().getContext()->CSSetConstantBuffers(5, 1, &batchSizeBuffer);
		D3D11Graphics::getInstance().getContext()->UpdateSubresource(cloudCentreBuffer, 0, NULL, &position, 0, 0);
		D3D11Graphics::getInstance().getContext()->CSSetConstantBuffers(9, 1, &cloudCentreBuffer);
	}
}

//--------------------------------------------------------------------------------------
// Set topology and sampler for object and draw
//--------------------------------------------------------------------------------------
void ParticleCloudObject::Render()
{
	D3D11Graphics::getInstance().getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	D3D11Graphics::getInstance().SetSamplerLinear();

	D3D11Graphics::getInstance().getContext()->Draw(vertices.size(), 0);
}

//--------------------------------------------------------------------------------------
// Write profile data to file
//--------------------------------------------------------------------------------------
void ParticleCloudObject::WriteDataToFile(std::string fileName)
{
	std::ofstream profileDataFile;
	profileDataFile.open(fileName, std::ios_base::app);

	profileDataFile << "==================================================\n";
	profileDataFile << "Object: " << name << "\n";
	profileDataFile << "Type: " << typeid(this).name() << "\n";
	std::string updateLocString;
	switch (updateLocation)
	{
	case CPU: updateLocString = "CPU";
		break;
	case CPU_MULTITHREADED: updateLocString = "CPU_MULTITHREADED";
		break;
	case GPU: updateLocString = "GPU";
		break;
	}
	profileDataFile << "Using: " << updateLocString << "\n";
	profileDataFile << "Iterations: " << stopWatch->getIterations() << "\n";
	profileDataFile << "Average Update Time: " << stopWatch->getAverage() << " milliseconds\n";

	profileDataFile.close();
}

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void ParticleCloudObject::CleanUp()
{
	GameObject::CleanUp();
	geometryShader->Release();
	texture->Release();

	if (updateLocation == Location::CPU_MULTITHREADED)
		delete threadPool;
	else if (updateLocation == Location::GPU)
	{
		computeShader->Release();
		computeInputBuffer->CleanUp();
		computeOutputBuffer->CleanUp();
		batchSizeBuffer->Release();
		cloudCentreBuffer->Release();
	}
}