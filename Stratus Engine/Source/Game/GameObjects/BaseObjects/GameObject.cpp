#include <fstream>
#include <Game\GameObjects\BaseObjects\GameObject.h>
#include <Graphics\D3D11Graphics.h>

using namespace DirectX;
using namespace std;

//--------------------------------------------------------------------------------------
// Base Gameobject constructors stores a reference to graphics for creation of resources
//--------------------------------------------------------------------------------------
GameObject::GameObject(){}

//--------------------------------------------------------------------------------------
// Base Initialize function, creates variables / buffers used by all child classes
//--------------------------------------------------------------------------------------
HRESULT GameObject::Initialise(string name, XMFLOAT3 translation, XMFLOAT3 rotation, float scale, const string& shaderFile)
{
	this->name = name;
	stopWatch = new Stopwatch();
	string fullShaderFilePath = "Shaders/" + shaderFile;
	shaderFileName = wstring(fullShaderFilePath.begin(), fullShaderFilePath.end());

	if (FAILED(ShaderFactory::CreateVertexShader(&vertexShader, shaderFileName, "VS", layout, arraySize, &inputLayout)))
		return E_FAIL;
	if (FAILED(ShaderFactory::CreateShader<ID3D11PixelShader>(&pixelShader, shaderFileName, "PS", "ps_4_0")))
		return E_FAIL;

	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthClipEnable = true;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	D3D11Graphics::getInstance().getDevice()->CreateRasterizerState(&rasterizerDesc, &rasterizer);

	this->translation = translation;
	this->rotation = rotation;
	this->scale = XMFLOAT3(scale, scale, scale);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Base clean up function to destroy resources
//--------------------------------------------------------------------------------------
void GameObject::CleanUp()
{
	vertexShader->Release();
	pixelShader->Release();
	delete stopWatch;

	if(vertexBuffer) vertexBuffer->Release();
	if(indexBuffer) indexBuffer->Release();
}

//--------------------------------------------------------------------------------------
// Basic update function to be overriden by subclasses
//--------------------------------------------------------------------------------------
void GameObject::Update(float previousFrameDuration) { }

//--------------------------------------------------------------------------------------
// Update rasterizer to toggle wireframe mode
//--------------------------------------------------------------------------------------
void GameObject::ToggleWireframe()
{
	if (rasterizerDesc.FillMode == D3D11_FILL_SOLID)
		rasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;
	else
		rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	D3D11Graphics::getInstance().getDevice()->CreateRasterizerState(&rasterizerDesc, &rasterizer);
}

//--------------------------------------------------------------------------------------
// Update rasterizer to toggle antialiasing
//--------------------------------------------------------------------------------------
void GameObject::ToggleAntialiasing()
{
	if (rasterizerDesc.AntialiasedLineEnable == TRUE)
		rasterizerDesc.AntialiasedLineEnable = FALSE;
	else
		rasterizerDesc.AntialiasedLineEnable = TRUE;
	D3D11Graphics::getInstance().getDevice()->CreateRasterizerState(&rasterizerDesc, &rasterizer);
}

//--------------------------------------------------------------------------------------
// Base function to set the world constant buffer and base shaders to be active
//--------------------------------------------------------------------------------------
void GameObject::SetShadersAndBuffers()
{
	SetWorldConstBuffer();
	
	D3D11Graphics::getInstance().getContext()->RSSetState(rasterizer);

	D3D11Graphics::getInstance().getContext()->IASetInputLayout(inputLayout);
	D3D11Graphics::getInstance().getContext()->VSSetShader(vertexShader, NULL, 0);
	D3D11Graphics::getInstance().getContext()->PSSetShader(pixelShader, NULL, 0);
}

//--------------------------------------------------------------------------------------
// Sets world buffer for game object to be active
//--------------------------------------------------------------------------------------
void GameObject::SetWorldConstBuffer()
{
	WorldMatrix worldMatrix;
	worldMatrix.World = XMMatrixScaling(scale.x, scale.y, scale.z) *
						XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z) *
						XMMatrixTranslation(translation.x, translation.y, translation.z);

	D3D11Graphics::getInstance().getContext()->UpdateSubresource(D3D11Graphics::getInstance().getWorldBuffer(), 0, NULL, &worldMatrix, 0, 0);
}

//--------------------------------------------------------------------------------------
// Checks if a file exists (checking for textures etc)
//--------------------------------------------------------------------------------------
bool GameObject::exists(const char *fileName)
{
	ifstream infile(fileName);
	return infile.good();
}

//--------------------------------------------------------------------------------------
// Base function to write profile data to file
//--------------------------------------------------------------------------------------
void GameObject::WriteDataToFile(string fileName){}
