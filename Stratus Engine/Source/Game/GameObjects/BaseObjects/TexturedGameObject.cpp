#include <fstream>
#include <Graphics\D3D11Graphics.h>
#include <Game\GameObjects\BaseObjects\GameObject.h>
#include <Game\GameObjects\BaseObjects\TexturedGameObject.h>
#include <Utils\FileLoading\WICTextureLoader.h>

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
TexturedGameObject::TexturedGameObject() : GameObject(){}

//--------------------------------------------------------------------------------------
// Set up additional resources for Textured object (new vertex layout, textures)
//--------------------------------------------------------------------------------------
HRESULT TexturedGameObject::Initialise(const std::string& FileName, XMFLOAT3 translation, XMFLOAT3 rotation, float scale)
{
	D3D11_INPUT_ELEMENT_DESC newLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	arraySize = ARRAYSIZE(newLayout);
	layout = newLayout;

	if (FAILED(FileLoader::LoadOBJ("Models/" + FileName + "/MODEL.obj", vertices, indices)))
		return E_FAIL;

	if (FAILED(BufferFactory::CreateVertexBuffer<Vertex>(vertices, &vertexBuffer)))
		return E_FAIL;

	if (FAILED(BufferFactory::CreateIndexBuffer(indices, &indexBuffer)))
		return E_FAIL;

	if (FAILED(GameObject::Initialise(FileName, translation, rotation, scale, "TexturedShader.fx")))
		return E_FAIL;

	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	D3D11Graphics::getInstance().getDevice()->CreateRasterizerState(&rasterizerDesc, &rasterizer);

	const std::string FilePath = "Models/" + FileName + "/";

	std::string t = FilePath + "COLOR.png";
	if (!exists(t.c_str()))
		t = dt;
	if (FAILED(CreateWICTextureFromFile(D3D11Graphics::getInstance().getDevice(), D3D11Graphics::getInstance().getContext(), std::wstring(t.begin(), t.end()).c_str(), nullptr, &texture)))
		return E_FAIL;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void TexturedGameObject::SetShadersAndBuffers()
{
	GameObject::SetShadersAndBuffers();
	UINT stride = sizeof(Vertex); UINT offset = 0;
	D3D11Graphics::getInstance().getContext()->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	D3D11Graphics::getInstance().getContext()->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	D3D11Graphics::getInstance().getContext()->GSSetShader(NULL, NULL, 0);
	D3D11Graphics::getInstance().getContext()->PSSetShaderResources(0, 1, &texture);
}

//--------------------------------------------------------------------------------------
// Set topology and sampler for object and draw
//--------------------------------------------------------------------------------------
void TexturedGameObject::Render()
{
	D3D11Graphics::getInstance().getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	D3D11Graphics::getInstance().SetSamplerLinear();

	D3D11Graphics::getInstance().getContext()->DrawIndexed(indices.size(), 0, 0);
}

void TexturedGameObject::WriteDataToFile(std::string fileName) { }

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void TexturedGameObject::CleanUp()
{
	GameObject::CleanUp();
	texture->Release();
}