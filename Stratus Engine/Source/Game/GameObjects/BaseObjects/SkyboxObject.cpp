#include <fstream>
#include <Graphics\D3D11Graphics.h>
#include <Game\GameObjects\BaseObjects\GameObject.h>
#include <Game\GameObjects\BaseObjects\SkyboxObject.h>
#include <Utils\FileLoading\DDSTextureLoader.h>

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Call base constructor for Game Object
//--------------------------------------------------------------------------------------
SkyboxObject::SkyboxObject() : GameObject(){}

//--------------------------------------------------------------------------------------
// Set up additional resources for Environment Map object (new vertex layout, textures)
//--------------------------------------------------------------------------------------
HRESULT SkyboxObject::Initialise(WeatherType type, XMFLOAT3 translation, XMFLOAT3 rotation, float scale)
{
	D3D11_INPUT_ELEMENT_DESC newLayout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	arraySize = ARRAYSIZE(newLayout);
	layout = newLayout;

	if (FAILED(FileLoader::LoadOBJ("Models/SkyBox/MODEL.obj", vertices, indices)))
		return E_FAIL;

	if (FAILED(BufferFactory::CreateVertexBuffer<Vertex>(vertices, &vertexBuffer)))
		return E_FAIL;

	if (FAILED(BufferFactory::CreateIndexBuffer(indices, &indexBuffer)))
		return E_FAIL;

	if (FAILED(GameObject::Initialise("Skybox", translation, rotation, scale, "SkyBoxShader.fx")))
		return E_FAIL;

	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	D3D11Graphics::getInstance().getDevice()->CreateRasterizerState(&rasterizerDesc, &rasterizer);

	std::string textureName = "";

	switch (type)
	{
	case Clear:
		textureName = "Clear.dds";
		break;
	case Overcast:
		textureName = "Overcast.dds";
		break;
	case Stormy:
		textureName = "Stormy.dds";
		break;
	}

	std::string cm = "Models/SkyBox/" + textureName;
	if (exists(cm.c_str()))
	{
		if (FAILED(CreateDDSTextureFromFile(D3D11Graphics::getInstance().getDevice(), std::wstring(cm.begin(), cm.end()).c_str(), nullptr, &cubeMap)))
			return E_FAIL;
	}
	else
	{
		return E_FAIL;
	}
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set additional resources to be active (shaders, textures)
//--------------------------------------------------------------------------------------
void SkyboxObject::SetShadersAndBuffers()
{
	GameObject::SetShadersAndBuffers();
	UINT stride = sizeof(Vertex); UINT offset = 0;
	D3D11Graphics::getInstance().getContext()->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	D3D11Graphics::getInstance().getContext()->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	D3D11Graphics::getInstance().getContext()->GSSetShader(NULL, NULL, 0);
	D3D11Graphics::getInstance().getContext()->PSSetShaderResources(1, 1, &cubeMap);
}

//--------------------------------------------------------------------------------------
// Set topology for object and draw
//--------------------------------------------------------------------------------------
void SkyboxObject::Render()
{
	D3D11Graphics::getInstance().getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	D3D11Graphics::getInstance().getContext()->DrawIndexed(indices.size(), 0, 0);
}

void SkyboxObject::WriteDataToFile(std::string fileName) { }

//--------------------------------------------------------------------------------------
// Clean up resources
//--------------------------------------------------------------------------------------
void SkyboxObject::CleanUp()
{
	GameObject::CleanUp();
	cubeMap->Release();
}