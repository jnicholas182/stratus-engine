#include <Game\GameObjects\WeatherObjects\SimplexCloudObject.h> //must go first to avoid boost winsock re-include error

#include <Mmsystem.h>
#include <fstream>
#include <iostream>
#include <ctime>
#include <Graphics\D3D11Graphics.h>
#include <Graphics\Camera.h>
#include <Input\GamePad.h>
#include <Input\Input.h>

#include <Game\Game.h>
#include <Game\GameObjects\BaseObjects\GameObject.h>
#include <Game\GameObjects\BaseObjects\SkyboxObject.h>
#include <Game\GameObjects\BaseObjects\TexturedGameObject.h>
#include <Game\GameObjects\WeatherObjects\RainEmitter.h>
#include <Game\GameObjects\WeatherObjects\SnowEmitter.h>
#include <Game\GameObjects\WeatherObjects\FogEmitter.h>
#include <Game\GameObjects\WeatherObjects\ParticleCloudObject.h>
#include <Game\GameObjects\WeatherObjects\LightningObject.h>

using namespace std;
using namespace DirectX;

//--------------------------------------------------------------------------------------
// Create Game and store reference to graphics to create resources
//--------------------------------------------------------------------------------------
Game::Game(){}

//--------------------------------------------------------------------------------------
// Initialise game and resource
//--------------------------------------------------------------------------------------
HRESULT Game::Initialise()
{
	gamepad = new Gamepad(1);
	if (FAILED(Camera::getInstance().Initialise(XMFLOAT3(0.0f, 3.0f, 0.0f), XMFLOAT3(0.0f, -1.0f, 0.0f))))
		return E_FAIL;

	if (FAILED(SetupGameObjects()))
		return E_FAIL;
	
	if (FAILED(SetupLights()))
		return E_FAIL;

	if (FAILED(BufferFactory::CreateConstantBuffer<XMFLOAT4>(&previousFrameDurationBuffer)))
		return E_FAIL;
	D3D11Graphics::getInstance().getContext()->CSSetConstantBuffers(8, 1, &previousFrameDurationBuffer);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Run game in loop
//--------------------------------------------------------------------------------------
MSG Game::Run(HWND g_hWnd)
{
	MSG msg = { 0 };
	float previousFrameDuration = 0.0f;
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			// Start update timer
			unsigned int startTime = timeGetTime();

			//Update previous frame duration for compute shaders
			D3D11Graphics::getInstance().getContext()->UpdateSubresource(previousFrameDurationBuffer, 0, NULL, &previousFrameDuration, 0, 0);

			// Clear back buffer and depth stencil
			D3D11Graphics::getInstance().ClearBackBuffer();
			D3D11Graphics::getInstance().ClearDepthStencil();

			//If focus is on game window, process input
			if (GetFocus() == g_hWnd)
			{
				// Gamepad update and input handling
				gamepad->Update();
				Input::ReceiveControllerInput(previousFrameDuration, g_hWnd, gamepad, gameObjects, skyBox);

				// Handle Keyboard Input
				Input::ReceiveKeyboardInput(previousFrameDuration, g_hWnd, gameObjects, skyBox);
			}
			// Update Camera based on input
			Camera::getInstance().Update(previousFrameDuration);
			
			// Render skybox then clear depth stencil so objects render in front of skybox regardless of size
			skyBox->SetShadersAndBuffers();
			skyBox->Update(previousFrameDuration);
			skyBox->Render();
			D3D11Graphics::getInstance().ClearDepthStencil();

			// Render every game object
			for (unsigned int i = 0; i < gameObjects.size(); i++)
			{
				gameObjects[i]->SetShadersAndBuffers();
				gameObjects[i]->Update(previousFrameDuration);
				gameObjects[i]->Render();
			}

			// Pass lights to shader
			D3D11Graphics::getInstance().getContext()->UpdateSubresource(LightDataBuffer, 0, NULL, lights, 0, 0);

			// Present the swap chain
			D3D11Graphics::getInstance().PresentSwapChain();

			// End timer and store time taken for update
			previousFrameDuration = (timeGetTime() - startTime) / 1000.0f;
		}
	}
	return msg;
}

//--------------------------------------------------------------------------------------
// Clean up GameObjects
//--------------------------------------------------------------------------------------
void Game::CleanUp()
{
	struct tm now;
	time_t t = time(0);
	localtime_s(&now, &t);

	ofstream profileDataFile;
	string fileName = "PROFILE_" + to_string(now.tm_mday) + 
								   to_string(now.tm_mon + 1) + 
								   to_string(now.tm_year + 1900) + "_" + 
								   to_string(now.tm_hour) + 
								   to_string(now.tm_min) + 
								   to_string(now.tm_sec) + ".txt";
	profileDataFile.open(fileName);
	profileDataFile.close();
	for each (GameObject* gameObject in gameObjects)
		gameObject->WriteDataToFile(fileName);

	Camera::getInstance().CleanUp();
	for (unsigned int i = 0; i < gameObjects.size(); i++)
		gameObjects[i]->CleanUp();
	LightDataBuffer->Release();
	previousFrameDurationBuffer->Release();
}

//--------------------------------------------------------------------------------------
// Create and Load object for scene
//--------------------------------------------------------------------------------------
HRESULT Game::SetupGameObjects()
{	
	skyBox = new SkyboxObject();
	if (FAILED(skyBox->Initialise(WeatherType::Clear)))
		return E_FAIL;

	TexturedGameObject* floor = new TexturedGameObject();
	if (!FAILED(floor->Initialise("Floor")))
		gameObjects.push_back(floor);

	SimplexCloudObject* perlinClouds = new SimplexCloudObject();
	if (!FAILED(perlinClouds->Initialise(Location::GPU, "SimplexClouds1", CloudCover::Cloud_Overcast, 200.0f)))
		gameObjects.push_back(perlinClouds);

	ParticleCloudObject* rainCloud = new ParticleCloudObject();
	if (!FAILED(rainCloud->Initialise(Location::GPU, "RainCloud", Size::Small, XMFLOAT3(-20.0f, 40.0f, -20.0f))))
		gameObjects.push_back(rainCloud);

	RainEmitter* rain = new RainEmitter();
	if (!FAILED(rain->Initialise(Location::GPU, "RainEmitter", rainCloud, FallType::Heavy)))
		gameObjects.push_back(rain);
		
	ParticleCloudObject* snowCloud = new ParticleCloudObject();
	if (!FAILED(snowCloud->Initialise(Location::GPU, "SnowCloud", Size::Small, XMFLOAT3(20.0f, 40.0f, 20.0f))))
		gameObjects.push_back(snowCloud);

	SnowEmitter* snow = new SnowEmitter();
	if (!FAILED(snow->Initialise(Location::GPU, "SnowEmitter", snowCloud, FallType::Heavy)))
		gameObjects.push_back(snow);

	FogEmitter* fog = new FogEmitter();
	if (!FAILED(fog->Initialise(Location::GPU, "FogEmitter", Density::Average, XMFLOAT3(-20.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.1f, 20.0f))))
		gameObjects.push_back(fog);

	ParticleCloudObject* lightningCloud = new ParticleCloudObject();
	if (!FAILED(lightningCloud->Initialise(Location::GPU, "LightningCloud", Size::Small, XMFLOAT3(20.0f, 40.0f, -20.0f))))
		gameObjects.push_back(lightningCloud);

	LightningObject* lightning = new LightningObject();
	if (!FAILED(lightning->Initialise("Lightning", lightningCloud)))
		gameObjects.push_back(lightning);
		
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Create lights in scene
//--------------------------------------------------------------------------------------
HRESULT Game::SetupLights()
{
	HRESULT hr;
	hr = BufferFactory::CreateConstantBuffer<LightData>(&LightDataBuffer);
	if (FAILED(hr))
		return hr;

	lights = new LightData();
	lights->lightCount = 0;
	
	//Create white directional light to demonstrate parallel light rays
	AddLight(lights, XMFLOAT4(0.0f, 100.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, -1.0f, 0.0f, 1.0f), XMFLOAT4(0.6f, 0.6f, 0.6f, 1.0f), 1);

	D3D11Graphics::getInstance().getContext()->PSSetConstantBuffers(4, 1, &LightDataBuffer);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Add light data to light data structure to be passed to shaders
//--------------------------------------------------------------------------------------
void Game::AddLight(LightData* p_this, DirectX::XMFLOAT4 Position, DirectX::XMFLOAT4 Direction, DirectX::XMFLOAT4 Colour, int type, float cone)
{
	p_this->Position[p_this->lightCount] = Position;
	p_this->Direction[p_this->lightCount] = Direction;
	p_this->Diffuse[p_this->lightCount] = Colour;
	p_this->Specular[p_this->lightCount] = Colour;
	p_this->type[p_this->lightCount].x = type; //Type: 0 = point, 1 = directional, 2 = spotlight
	p_this->cone[p_this->lightCount] = cone;
	p_this->lightCount++;
}