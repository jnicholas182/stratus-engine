#include <directxmath.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <Utils\FileLoading\FileLoader.h>

using namespace std;
using namespace DirectX;

//--------------------------------------------------------------------------------------
// Loader for OBJ files containing vertices, normals and texture coordinates
//--------------------------------------------------------------------------------------
HRESULT FileLoader::LoadOBJ(const string& fileName, vector<Vertex>& objectVertices, vector<unsigned int>& indices)
{
	ifstream inputFile;
	inputFile.open(fileName);

	if (!inputFile.good())
		return E_FAIL;

	int triangleCount = 0;
	vector<XMFLOAT3> vertices;
	vector<XMFLOAT3> normals;
	vector<XMFLOAT2> texCoords;

	string lineIn;
	vector<string> splitLine;

	while (!inputFile.eof())
	{
		getline(inputFile, lineIn);
		split(lineIn, splitLine, ' ');

		if (splitLine.size() > 0)
		{
			if (splitLine[0].at(0) == 'v')
			{
				if (splitLine[0].size() == 1)
					vertices.push_back(XMFLOAT3( stof(splitLine[1]), stof(splitLine[2]), stof(splitLine[3]) ));
				else
				{
					switch (splitLine[0].at(1))
					{
					case 'n':
						normals.push_back(XMFLOAT3( stof(splitLine[1]), stof(splitLine[2]), stof(splitLine[3]) ));
						break;

					case 't':
						texCoords.push_back(XMFLOAT2( stof(splitLine[1]), stof(splitLine[2]) ));
						break;
					}
				}
			}
			else if (splitLine[0].at(0) == 'f')
			{
				vector<string> splitIndices;
				for (int i = 1; i < 4; i++)
				{
					split(splitLine[i], splitIndices, '/');
					objectVertices.push_back(Vertex{ vertices[stoi(splitIndices[0]) - 1], normals[stoi(splitIndices[2]) - 1], texCoords[stoi(splitIndices[1]) - 1] });
				}
				triangleCount++;
			}
		}
	}

	for (int i = 0; i < triangleCount * 3; i++)
	{
		indices.push_back(i);
	}

	//Close the file
	inputFile.close();

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Split read string into individual strings based on entered delimiter char
//--------------------------------------------------------------------------------------
void FileLoader::split(const std::string &textIn, std::vector<std::string> &splitText, char delimiter)
{
	splitText.clear();
	stringstream ss(textIn);
	string item;
	while (getline(ss, item, delimiter)) 
	{
		if (item != "")
			splitText.push_back(item);
	}
}