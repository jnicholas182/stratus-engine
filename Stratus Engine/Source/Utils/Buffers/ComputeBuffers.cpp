#include <Utils\Buffers\ComputeBuffers.h>
#include <Graphics\D3D11Graphics.h>

//--------------------------------------------------------------------------------------
// Clear all compute buffers
//--------------------------------------------------------------------------------------
void ComputeBuffers::ClearBuffers()
{
	void* blank = nullptr;
	D3D11Graphics::getInstance().getContext()->CSSetUnorderedAccessViews(0, 1, (ID3D11UnorderedAccessView**)&blank, nullptr);
	D3D11Graphics::getInstance().getContext()->CSSetShaderResources(0, 1, (ID3D11ShaderResourceView**)&blank);
	D3D11Graphics::getInstance().getContext()->CSSetShaderResources(1, 1, (ID3D11ShaderResourceView**)&blank);
}

//--------------------------------------------------------------------------------------
// Create a compute input buffer
//--------------------------------------------------------------------------------------
ComputeInputBuffer::ComputeInputBuffer(){}

//--------------------------------------------------------------------------------------
// Set buffer to be actuve
//--------------------------------------------------------------------------------------
void ComputeInputBuffer::SetActive(int bufferRegistration)
{
	D3D11Graphics::getInstance().getContext()->CSSetShaderResources(bufferRegistration, 1, &m_srcDataGPUBufferView);
}

//--------------------------------------------------------------------------------------
// Clean up buffer
//--------------------------------------------------------------------------------------
void ComputeInputBuffer::CleanUp()
{
	m_srcDataGPUBuffer->Release();
	m_srcDataGPUBufferView->Release();
}

//--------------------------------------------------------------------------------------
// Create a compute output buffer
//--------------------------------------------------------------------------------------
ComputeOutputBuffer::ComputeOutputBuffer(){}

//--------------------------------------------------------------------------------------
// Clean up buffer
//--------------------------------------------------------------------------------------
void ComputeOutputBuffer::CleanUp()
{
	m_destDataGPUBuffer->Release();
	m_destDataGPUBufferView->Release();
}

//--------------------------------------------------------------------------------------
// Initialise compute output buffer
//--------------------------------------------------------------------------------------
HRESULT ComputeOutputBuffer::Initialise(UINT elementCount, UINT stride)
{
	this->byteSize = elementCount * stride;

	// The compute shader will need to output to some buffer so here we create a GPU buffer for that.
	D3D11_BUFFER_DESC descGPUBuffer;
	ZeroMemory(&descGPUBuffer, sizeof(descGPUBuffer));
	descGPUBuffer.Usage = D3D11_USAGE_DEFAULT;
	descGPUBuffer.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
	descGPUBuffer.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	descGPUBuffer.ByteWidth = elementCount * stride;
	descGPUBuffer.StructureByteStride = stride;

	if (FAILED(D3D11Graphics::getInstance().getDevice()->CreateBuffer(&descGPUBuffer, NULL, &m_destDataGPUBuffer)))
		return E_FAIL;

	// The view we need for the output is an unordered access view. This is to allow the compute shader to write anywhere in the buffer.
	D3D11_UNORDERED_ACCESS_VIEW_DESC descView;
	ZeroMemory(&descView, sizeof(descView));
	descView.Format = DXGI_FORMAT_UNKNOWN;
	descView.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	descView.Buffer.NumElements = elementCount;
	descView.Buffer.FirstElement = 0;	

	if (FAILED(D3D11Graphics::getInstance().getDevice()->CreateUnorderedAccessView(m_destDataGPUBuffer, &descView, &m_destDataGPUBufferView)))
		return E_FAIL;

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Set buffer to be active
//--------------------------------------------------------------------------------------
void ComputeOutputBuffer::SetActive()
{
	D3D11Graphics::getInstance().getContext()->CSSetUnorderedAccessViews(0, 1, &m_destDataGPUBufferView, NULL);
}