#include <Utils\ShaderFactory.h>

//--------------------------------------------------------------------------------------
// Create Vertex shader and input layout
//--------------------------------------------------------------------------------------
HRESULT ShaderFactory::CreateVertexShader(ID3D11VertexShader** shader, std::wstring shaderFile, std::string shaderName, D3D11_INPUT_ELEMENT_DESC* layout, UINT numElements, ID3D11InputLayout** vertexLayout)
{
	ID3DBlob* shaderBlob = NULL;

	// Compile the vertex shader
	if (FAILED(CompileShaderFromFile(shaderFile.c_str(), shaderName.c_str(), "vs_4_0", &shaderBlob)))
	{
		MessageBox(NULL, "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK);
		return E_FAIL;
	}

	// Create the vertex shader
	if (FAILED(D3D11Graphics::getInstance().getDevice()->CreateVertexShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), NULL, shader)))
	{
		shaderBlob->Release();
		return E_FAIL;
	}

	// Create the input layout
	if (FAILED(D3D11Graphics::getInstance().getDevice()->CreateInputLayout(layout, numElements, shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), vertexLayout)))
	{
		shaderBlob->Release();
		return E_FAIL;
	}

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Compile shaders from FX files
//--------------------------------------------------------------------------------------
HRESULT ShaderFactory::CompileShaderFromFile(LPCWSTR szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
{
	HRESULT hr = S_OK;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

	ID3DBlob* pErrorBlob;
	hr = D3DCompileFromFile(szFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, szEntryPoint, szShaderModel,
		dwShaderFlags, 0, ppBlobOut, &pErrorBlob);
	if (FAILED(hr))
	{
		if (pErrorBlob != NULL)
			OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());
		if (pErrorBlob) pErrorBlob->Release();
		return hr;
	}
	if (pErrorBlob) pErrorBlob->Release();

	return S_OK;
}