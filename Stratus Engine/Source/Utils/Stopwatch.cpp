#include <Utils\Stopwatch.h>

//--------------------------------------------------------------------------------------
// Create Stopwatch
//--------------------------------------------------------------------------------------
Stopwatch::Stopwatch()
{
	iterations = 0;
	timeTotal = 0.0f;
}

//--------------------------------------------------------------------------------------
// Start a timer
//--------------------------------------------------------------------------------------
void Stopwatch::Start()
{
	start = system_clock::now();
}

//--------------------------------------------------------------------------------------
// Stop the timer and add time elapsed to total and increment iteration count
//--------------------------------------------------------------------------------------
void Stopwatch::Stop()
{
	end = system_clock::now();
	duration<float> updateElapsed = end - start;
	timeTotal += updateElapsed.count();
	iterations++;
}

//--------------------------------------------------------------------------------------
// Calculate average time in milliseconds by dividing total time by iteration count
//--------------------------------------------------------------------------------------
float Stopwatch::getAverage()
{
	return (timeTotal*1000.0f) / iterations;
}

//--------------------------------------------------------------------------------------
// Return iteration count
//--------------------------------------------------------------------------------------
int Stopwatch::getIterations()
{
	return iterations;
}