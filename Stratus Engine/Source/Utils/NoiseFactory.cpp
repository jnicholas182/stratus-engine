#include <cmath>
#include <random>
#include <algorithm>
#include <numeric>

#include <Utils\NoiseFactory.h>

//--------------------------------------------------------------------------------------
// Create new Simplex noise factory
//--------------------------------------------------------------------------------------
SimplexNoise::SimplexNoise(unsigned int seed, float cover, float sharpness) : cover(cover), sharpness(sharpness)
{
	// Fill permutation table with values from 0 to 255
	permutation.resize(256);
	std::iota(permutation.begin(), permutation.end(), 0);

	// Initialize random engine with seed and shuffle permutation table using engine
	std::default_random_engine engine(seed);
	std::shuffle(permutation.begin(), permutation.end(), engine);

	// Duplicate the permutation vector
	permutation.insert(permutation.end(), permutation.begin(), permutation.end());
}

//--------------------------------------------------------------------------------------
// Function to apply expontential filter to raw noise values
//--------------------------------------------------------------------------------------
double SimplexNoise::exponentialFilter(float perlinValue)
{
	float c = (255.0f * perlinValue) - (255.0f - cover);
	if (c < 0.0f)
		c = 0.0f;

	return floor(255.0f - ((float)(pow(sharpness, c))*255.0f));
}

//--------------------------------------------------------------------------------------
// Return noise value after stacking octaves and applying exponential filter
//--------------------------------------------------------------------------------------
double SimplexNoise::getNoiseValue(int octaves, float persistence, float scale, double x, double y, double z)
{
	float total = 0;
	float frequency = scale;
	float amplitude = 1;
	float maxAmplitude = 0;

	for (int i = 0; i < octaves; i++) {
		total += (float)raw3DNoise(x * frequency, y * frequency, z * frequency) * amplitude;

		frequency *= 2;
		maxAmplitude += amplitude;
		amplitude *= persistence;
	}

	return exponentialFilter(total / maxAmplitude) / 255.0f;
}

//--------------------------------------------------------------------------------------
// Return raw noise value at coordinates x, y, z
//--------------------------------------------------------------------------------------
double SimplexNoise::raw3DNoise(double x, double y, double z)
{
	// Find unit cube containing point x, y, z
	int X = (int)floor(x) & 255;
	int Y = (int)floor(y) & 255;
	int Z = (int)floor(z) & 255;
	
	// Find relative x, y, z of point in unit cube
	x -= floor(x);
	y -= floor(y);
	z -= floor(z);

	// Compute fade curves
	double u = fade(x);
	double v = fade(y);
	double w = fade(z);

	// Hash coordinates of unit cube corners
	int A = permutation[X] + Y;
	int AA = permutation[A] + Z;
	int AB = permutation[A + 1] + Z;
	int B = permutation[X + 1] + Y;
	int BA = permutation[B] + Z;
	int BB = permutation[B + 1] + Z;

	// Add blended results from corners of unit cube
	double res = lerp(w, lerp(v, lerp(u, grad(permutation[AA], x, y, z), 
										 grad(permutation[BA], x - 1, y, z)), 
								 lerp(u, grad(permutation[AB], x, y - 1, z), 
								         grad(permutation[BB], x - 1, y - 1, z))),
						 lerp(v, lerp(u, grad(permutation[AA + 1], x, y, z - 1), 
										 grad(permutation[BA + 1], x - 1, y, z - 1)), 
								 lerp(u, grad(permutation[AB + 1], x, y - 1, z - 1), 
										 grad(permutation[BB + 1], x - 1, y - 1, z - 1))));
	// Return value between 0 and 1
	return (res + 1.0) / 2.0;
}

//--------------------------------------------------------------------------------------
// Function to return fade curve of value
//--------------------------------------------------------------------------------------
double SimplexNoise::fade(double t)
{
	return t * t * t * (t * (t * 6 - 15) + 10);
}

//--------------------------------------------------------------------------------------
// Linearly interpolate between 2 values
//--------------------------------------------------------------------------------------
double SimplexNoise::lerp(double t, double a, double b)
{
	return a + t * (b - a);
}

//--------------------------------------------------------------------------------------
// Convert lower 4 bits of hash into 12 gradient directions
//--------------------------------------------------------------------------------------
double SimplexNoise::grad(int hash, double x, double y, double z)
{
	int h = hash & 15;
	double u = h < 8 ? x : y, v = h < 4 ? y : h == 12 || h == 14 ? x : z;
	return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}